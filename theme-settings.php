<?php
/**
 * @file
 * Theme settings callbacks for learnline4 theme.
 */

function learnline4_form_system_theme_settings_alter(&$form, &$form_state) {
  $form['learnline4_html_meta_tags'] = array(
    '#type' => 'fieldset',
    '#title' => t('HTML Meta Tags'),
  );
  $form['learnline4_html_meta_tags']['learnline4_html_meta_description'] = array(
    '#type' => 'textfield',
    '#title' => t('Meta "description"'),
    '#description' => t('Will be only applied to the front page.'),
    '#default_value' => theme_get_setting('learnline4_html_meta_description') ?:
      'Startseite Bildungsportal NRW, Informationen des Schulministeriums NRW',
  );
  $form['learnline4_html_meta_tags']['learnline4_html_meta_keywords'] = array(
    '#type' => 'textfield',
    '#title' => t('Meta "keywords"'),
    '#description' => t('Will be only applied to the front page.'),
    '#default_value' => theme_get_setting('learnline4_html_meta_keywords') ?:
      'Kultusministerium,Schulministerium,Weiterbildungsministerium,MSW,NRW,Ministerium für Schule und Weiterbildung,Ministerium,Nordrhein,Westfalen,Nordrhein-Westfalen,Bildung,Schule,Weiterbildung,Lehrer,Lehrerin',
  );
  $form['learnline4_html_meta_tags']['learnline4_html_meta_author'] = array(
    '#type' => 'textfield',
    '#title' => t('Meta "author"'),
    '#default_value' => theme_get_setting('learnline4_html_meta_author') ?: 'MSW',
  );
  $form['learnline4_html_meta_tags']['learnline4_html_meta_copyright'] = array(
    '#type' => 'textfield',
    '#title' => t('Meta "copyright"'),
    '#default_value' => theme_get_setting('learnline4_html_meta_copyright') ?: 'MSW',
  );
  $form['learnline4_html_meta_tags']['learnline4_html_meta_robots'] = array(
    '#type' => 'textfield',
    '#title' => t('Meta "robots"'),
    '#default_value' => theme_get_setting('learnline4_html_meta_robots') ?: 'index, follow, noarchive',
  );
}
