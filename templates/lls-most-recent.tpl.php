<?php
// $id:$

/**
 * @file
 * Template file for displaying the latest learn:line NRW resources.
 */
?>
<?php if (isset($element['#children']) && is_array($element) && !empty($element['#children'])): ?>
  <?php foreach ($element['#children'] as $teaser): ?>
  <a href="<?php print $teaser['singlesearch']; ?>">
    <div class="lls-most-recent clearfix">
      <h4 class="lls-most-recent-header">
        <?php print $teaser['title']; ?>
      </h4>
      <?php if ($teaser['thumbnail']): ?>
      <img class="remote-src" data-img-src="<?php print $teaser['thumbnail']; ?>"/>
      <img src="<?php print lls_sodis_fallback_thumbnail($teaser); ?>" style="display: none;"/>
      <?php else: ?>
      <img src="<?php print lls_sodis_fallback_thumbnail($teaser); ?>"/>
      <?php endif; ?>
      <p class="lls-most-recent-content">
        <?php print lls_text_summary($teaser['description'], 90); ?>
      </p>
    </div>
  </a>
  <?php endforeach; ?>
<?php else: ?>
<div class="lls-most-recent">
  <p class="lls-most-recent-content">
    <?php print t('The most recent resources are currently unavailable.'); ?>
  </p>
</div>
<?php endif; ?>
