<?php
/**
 * @file
 * Template file for displaying the learn:line NRW search box.
 *
 * Variables:
 * - $base_url: Drupal's global base_url.
 * - $site_name: variable_get('site_name', '').
 * - $slogan: variable_get('site_slogan', '').
 * - $logo: theme_get_setting('logo_path') ?: theme_get_setting('logo').
 * - $search_term: Current search term or an empty string.
 * - $query: Current query parameters.
 * - $filter1: Array of search filter values.
 * - $filter2: Array of search filter values.
 */

$action = '/suche/';
$locations = lls_get_school_locations();
$locations_by_key = array_flip($locations);
$location_cols = lls_get_location_cols($locations);
$location_display = t('Mehr Treffer für Ihren Schulstandort');

if(empty($_GET['sl']) === false && isset($locations_by_key[$_GET['sl']]) === true) {
  lls_set_location_cookie($_GET['sl']);
}
?>
<!-- Modal Location -->
<div class="modal" id="modal-choose-location" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo t('Bitte wählen Sie Ihren Schulstandort im Kreis bzw. in der kreisfreien Stadt aus:'); ?></h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-3 col-md-3 col-sm-offset-1 col-md-offset-1">
            <?php print lls_get_location_row($location_cols, 0); ?>
          </div>
          <div class="col-sm-3 col-md-3">
            <?php print lls_get_location_row($location_cols, 1); ?>
          </div>
          <div class="col-sm-3 col-md-3">
            <?php print lls_get_location_row($location_cols, 2); ?>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button id="locationModalBtnClose" type="button" class="btn btn-default" data-dismiss="modal"><?php echo t('Schließen'); ?></button>
      </div>
    </div>
  </div>
</div>
<!-- /Modal Location -->

<form id="search-box" accept-charset="UTF-8" action="<?php print $action; ?>" method="get">
  <input id="primary-service" type="hidden" name="primary_service" value="sodis" disabled="disabled"/>
  <?php foreach (drupal_get_query_parameters($query, array('q', 'term', 'search')) as $preset_param => $preset_value): if (empty($preset_value)) continue; ?>
    <?php if (is_array($preset_value)): ?>
      <?php foreach ($preset_value as $pval): if (empty($pval)) continue; ?>
      <input class="preselection" type="hidden" name="<?php print $preset_param; ?>[]" value="<?php print htmlspecialchars($pval); ?>"/>
      <?php endforeach; ?>
    <?php else: ?>
    <input class="preselection" type="hidden" name="<?php print $preset_param; ?>" value="<?php print htmlspecialchars($preset_value); ?>"/>
    <?php endif; ?>
  <?php endforeach; ?>
  <div class="row">
    <div class="col left" id="search-logo-container">
      <a href="/" title="<?php print $slogan; ?>">
        <img src="<?php print $logo; ?>" alt="<?php print $site_name; ?>"/>
      </a>
    </div>
    <div class="subrow col left">
      <div class="col left" id="search-input-container">
        <div>
          <div id="search-input-wrapper">
            <input id="search-input" class="form-text required" tabindex="1" size="60" type="text" name="term" placeholder="Suche..." value="<?php print $search_term; ?>" />
          </div>
        </div>
      </div>
      <div class="col right" id="search-button-container">
        <div id="search-button-wrapper">
          <?php /* #0272C1 */ ?>
          <button id="search-button" type="submit" title="<?php t('Start searching'); ?>">
            <i class="fa fa-search"></i>
          </button>
        </div>
      </div>
      <?php if (!lls_is_page_suche()): ?>
      <div class="search-box-filter">
        <a href="#" class="choose-location" data-toggle="modal" data-target="#modal-choose-location"><span id="startLLSLocation"><?php echo $location_display; ?></span><i class="fa fa-external-link-square"></i></span></a>
        <span class="ttip" data-toggle="tooltip" data-placement="bottom" title="<?php echo t('Ihr zuständiges Medienzentrum stellt Ihnen über EDMOND NRW zusätzliche lokal lizensierte Lernmedien bereit. Bitte wählen Sie hierfür Ihren Schulstandort im Kreis bzw. in der kreisfreien Stadt aus.'); ?>"><span class="fa fa-stack"><i class="fa fa-circle-thin fa-stack-2x"></i><i class="fa fa-info fa-stack-1x"></i></span></span>
      </div>
      <?php endif; ?>
      <?php if (lls_is_page_suche()): ?>
      <div class="clearfix"></div>
      <div class="search-box-filter">
        <select name="contexts[]" class="select2 select-1" style="width: 49.5%" data-placeholder="Bildungsbereiche">
          <option></option>
          <?php foreach ($filter1 as $facet_value): ?>
            <?php
              if (empty($facet_value['term']) ||
                (isset($query['contexts']) && array_search($facet_value['term'], $query['contexts']) !== FALSE)) {
                continue;
              }
            ?>
            <option value="<?php print $facet_value['term']; ?>">
              <?php print $facet_value['term'] /*. ' (' . $facet_value['count'] . ')'*/; ?>
            </option>
          <?php endforeach; ?>
        </select>
        <select name="disciplines[0]" class="select2 select-2" style="width: 49.5%" data-placeholder="Fächer">
          <option></option>
          <?php foreach ($filter2 as $facet_value): ?>
            <?php
              if (empty($facet_value['term']) ||
                isset($query['disciplines']) && array_search($facet_value['term'], $query['disciplines']) !== FALSE) {
                continue;
              }
            ?>
            <option value="<?php print $facet_value['term']; ?>">
              <?php print $facet_value['term'] /*. ' (' . $facet_value['count'] . ')'*/; ?>
            </option>
          <?php endforeach; ?>
        </select>
      </div>
      <?php endif; ?>
    </div>
    <div class="row"></div>
  </div>
  <div class="clearfix"></div>
</form>

<script>var llsLocations = JSON.parse('<?php echo json_encode($locations_by_key); ?>');</script>
<script type="text/javascript">
(function($) {
  $(document).ready(function() {
    $('#search-button, #search-input').on('keyup', function(e) {
      if (e.keyCode == 13) {
        $('#search-box').find('.preselection').detach();
        $('#search-box').submit();
      }
    });
    $('#search-button').on('click', function() {
       $('#search-box').find('.preselection').detach();
       $('#search-box').submit();
    });
    $('.select2').each(function() {
      $(this).select2().on('select2:select', function() {
        $(this).parents('form').submit();
      });
    });

    setLLSLocation('startLLSLocation');
  });
})(jQuery);

function llsSetLocationHome(loc) {

  setLLSLocationCookie(loc);
  setLLSLocation('startLLSLocation');
  jQuery('.btn.btn-default[data-dismiss]').click();
}
</script>
