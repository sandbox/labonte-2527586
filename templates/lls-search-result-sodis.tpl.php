<?php
/**
 * @file
 * Template for a single SODIS result/hit.
 */
use \publicplan\lls\SodisHelper;

global $base_url;

// Clean result ID for use as HTML id attribute:
$htmlid = drupal_clean_css_identifier($result['id']);
// Determine a fallback thumbnail. This will be used, in two cases:
//  1. There is no thumbnail specified.
//  2. We detect (via JS), that the specified thumbnail cannot be loaded.
$theme_dir = drupal_get_path('theme', 'learnline4');
if ($primary_resourcetype = reset($result['learningResourceType_orig'])) {
  $primary_resourcetype = str_replace(' ', '_', $primary_resourcetype);
  $filename = 'Medientypen_'. $primary_resourcetype .'.png';
  $thumb_src = $theme_dir . '/img/' . $filename;
}
if ($primary_resourcetype && file_exists(drupal_realpath($thumb_src))) {
  $thumb_src = $base_url . '/' . $thumb_src;
} else {
  $thumb_src = $base_url . '/' . $theme_dir . '/img/Medientypen_Anderer_Ressourcentyp.png';
}

// Whitelist all attributes to show in the collapsed 'more' area:
$more_filters = array(
  array(
    'context' => 'Bildungsbereiche',
    'discipline' => 'Fach- und Sachgebiete',
  ),
  array(
    'learningResourceType_orig' => 'Medientypen',
    'age' => '.Lernalter',
    'keyword' => 'Schlüsselwörter',
    'language' => 'Sprachen',
    'copyright' => 'Urheberrecht',
  ),
  array(
    'competency' => '|mc|Medienkompetenz',
  ),
);
$more_attributes = array(
);

$media_competency = false;
if(isset($result['competency']) === true && count($result['competency']) > 0) {
  $media_competency = true;
}

// Determine the Creative Commons license image path:
$cc_logo = "{$base_url}/{$theme_dir}/" . SodisHelper::CREATIVE_COMMONS_ICONS_DIR . "/" . mb_strtolower($result['copyright']) . ".png";
$cc_logo_realpath = drupal_realpath("./{$theme_dir}/" . SodisHelper::CREATIVE_COMMONS_ICONS_DIR . "/" . mb_strtolower($result['copyright']) . ".png");

// Build relation tree for result:
$relation_tree = SodisHelper::relationTree($result);

?>
<div class="articlepreview sodis" id="<?php print $htmlid; ?>" data-id="<?php print $result['id']; ?>" data-url="<?php print $result['origin']; ?>" data-service="sodis">
  <div class="row">
    <div class="col-md-2 col-xs-3">
      <div class="thumbnailcontainer<?php echo (($media_competency === true) ? ' has-icon' : ''); ?>">
        <?php if (!empty($result['thumbnail'])): ?>
        <img class="img-responsive remote-src" data-img-src="<?php print $result['thumbnail']; ?>"/>
        <img class="img-responsive" src="<?php print $thumb_src; ?>" style="display: none;"/>
        <?php else: ?>
        <img class="img-responsive" src="<?php print $thumb_src; ?>"/>
        <?php endif; ?>
        <?php if ($media_competency === true): ?>
        <i></i>
        <?php endif; ?>
      </div>

      <p class="ressource_type"><?php print $result['learningResourceType'] ?: t('Anderer Ressourcentyp'); ?></p>

      <?php if(is_file($cc_logo_realpath)): ?>
        <div class="cc-icon">
          <img title="creative commons: <?php echo $result['copyright']; ?>" alt="Logo creative commons" src="<?php print $cc_logo; ?>"/>
        </div>
      <?php endif; ?>
    </div>
    <div class="col-md-10 col-xs-9">
      <div class="row">
        <div class="col-md-12 links edutags">

            <a class="sodis report-dead-link" href="" onclick="return false;"><?php echo t('Defekten Link melden'); ?></a> <a href="" class="share-link" onclick="return false;"><?php echo t('Teilen'); ?></a> <a href="" class="directlink-link" onclick="return false;"><?php echo t('Direktlink'); ?></a>

            <a class="rating" title="Bewertung abgeben" href="#" data-toggle="tooltip" data-placement="top" onclick="return false;">
              <span class="fa fa-star empty"></span>
              <span class="fa fa-star empty"></span>
              <span class="fa fa-star empty"></span>
              <span class="fa fa-star empty"></span>
              <span class="fa fa-star empty"></span>
            </a>

           <a class="rating-link" title="<?php echo t('Bewertung hinzufügen'); ?>" href="#" onclick="return false;"><?php echo t('Bewertung hinzufügen'); ?></a>

            <a class="edutags-toggle edutags" href="#" onclick="return false;">
              <span class="tags" title="<?php echo t('Tags hinzufügen'); ?>" data-toggle="tooltip" data-placement="top">&nbsp;</span>
              <span class="comments" title="<?php echo t('Kommentar hinzufügen'); ?>" data-toggle="tooltip" data-placement="top">&nbsp;</span>
            </a>

            <div class="tagbox">
              <img class="edutags-logo pull-right" src="<?php echo "/$theme_dir/img/edutags_logo.png"; ?>"/>

              <div class="tagbox_tags">
                <h5><?php echo t('Tags'); ?>:</h5><a class="edutags-button edutags-button-tag"><?php echo t('Tags hinzufügen'); ?></a>
                <ul></ul>
              </div>

              <div class="tagbox_comments">
                <h5><?php echo t('Kommentare'); ?>:</h5><a class="edutags-button edutags-button-comment"><?php echo t('Neuen Kommentar schreiben'); ?></a>
                <ul></ul>
              </div>

            </div>
        </div>
      </div>

	  <div class="row share">
	    <div class="col-md-12">
		  <div class="shariff" data-title="<?php print $result['title']; ?>" data-url="<?php print $base_url . $result['singlesearch']; ?>"></div>
	    </div>
	  </div>

	  <div class="row directlink">
	    <div class="col-md-12">
		  <div class="directlink-container"><input type="text" readonly="readonly" value="<?php print $base_url . $result['href']; ?>"/></div>
	    </div>
	  </div>

      <div class="row">
        <div class="col-md-12">
          <span class="provider">
            <?php if (strtoupper($result['provider']) === 'EDMOND'): ?>
            <img title="EDMOND NRW" alt="Logo EDMOND NRW" src="<?php print "{$base_url}/{$theme_dir}/img/edmond.jpg"; ?>"/>
            <?php endif; ?>
          </span>
          <?php if ($result['publisher']): ?>
          <h3><?php print implode(', ', $result['publisher']); ?></h3>
          <?php endif; ?>
          <h2 class="sodis-title"><a href="<?php print $result['href']; ?>" target="_blank" data-provider="<?php print strtoupper($result['provider']); ?>"><?php print $result['title']; ?></a></h2>
          <p class="sodis-description"><?php print $result['description']; ?></p>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 expand">
      <a class="btn btn-primary SeeMore collapsed" role="button" href="#more-<?php print $htmlid; ?>" data-toggle="collapse" aria-expanded="false" aria-controls="collapseMore">Mehr</a>
      <div id="more-<?php print $htmlid; ?>" class="collapse">
        <div class="accordioncontent">
          <div class="row">
            <?php foreach ($more_filters as $filter_group): ?>
              <div class="col-md-4 col-xs-12">
              <?php foreach ($filter_group as $attr_id => $attr_title): ?>
                <?php
                // Remove empty entries
                if (is_array($result[$attr_id])) {
                  $result[$attr_id] = array_filter($result[$attr_id], function ($value) {
                    return $value !== '';
                  });
                }
                ?>
                <?php if (substr($attr_title, 0, 1) === '.' && count($result[$attr_id]) > 0): ?>
                  <h4><?php print substr($attr_title, 1); ?></h4>
                  <?php if (is_array($result[$attr_id])): ?>
                    <?php foreach ($result[$attr_id] as $attr_value): ?>
                      <span class="<?php print $attr_id ?>"><?php print $attr_value; ?></span>
                    <?php endforeach; ?>
                  <?php else: ?>
                    <span class="<?php print $attr_id ?>"><?php print $result[$attr_id]; ?></span>
                  <?php endif; ?>
                <?php elseif (count($result[$attr_id]) > 0 && !empty($result[$attr_id])): ?>
                  <?php if (substr($attr_title, 0, 4) === '|mc|'): ?>
                    <h4 class="has-icon"><?php print substr($attr_title, 4); ?><button data-toggle="modal" data-target="#modal-media-competence" class="glyphicon glyphicon-info-sign modal-toggle"></button></h4>
                  <?php else: ?>
                    <h4><?php print $attr_title; ?></h4>
                  <?php endif; ?>
                    <?php if (is_array($result[$attr_id])): ?>
                      <?php foreach ($result[$attr_id] as $attr_value): ?>
                        <a href="<?php print SodisHelper::get()->linkFacet($attr_id, $attr_value); ?>" class="<?php print $attr_id ?>">
                          <?php print $attr_value; ?>
                        </a>
                      <?php endforeach; ?>
                    <?php else: ?>
                      <a href="<?php print SodisHelper::get()->linkFacet($attr_id, $result[$attr_id]); ?>" class="<?php print $attr_id ?>">
                        <?php print $result[$attr_id]; ?>
                      </a>
                    <?php endif; ?>
                <?php endif; ?>
              <?php endforeach; ?>
              </div>
            <?php endforeach; ?>
          </div>

          <?php if (!empty($relation_tree)): ?>
            <div class="row">
              <div class="col-md-12 border"><hr></div>
            </div>
            <div class="row">
              <div class="col-md-8">
                <h4><?php echo t('Dieses Material ist Teil einer Sammlung'); ?></h4>
                <ul class="tree">
                  <li<?php if ($relation_tree['parent']['id'] === $result['id']) { echo ' class="active"'; } ?>>
                    <a href="<?php print SodisHelper::searchLink($relation_tree['parent']['id']); ?>"><?php print check_plain($relation_tree['parent']['title']); ?></a>
                      <ul>
                        <?php foreach($relation_tree['children'] as $child): ?>
                          <li<?php if ($child['id'] === $result['id']) { echo ' class="active"'; } ?>>
                            <a href="<?php echo SodisHelper::searchLink($child['id']); ?>">
                              <?php echo check_plain(html_entity_decode($child['title'])); ?>
                            </a>
                          </li>
                        <?php endforeach; ?>
                      </ul>
                  </li>
                </ul>
              </div>
              <div class="col-md-4"></div>
            </div>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</div>
