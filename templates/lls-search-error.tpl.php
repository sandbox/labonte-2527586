<?php
/**
 * @file
 * Display error message in case of a failed search request.
 *
 * Variables:
 * - $service: String indicating the search service originating this error.
 * - $exception: The caught exception object.
 */
?>
<div class="lls-search-error jumbotron">
  <h1><?php print t('An error occurred!'); ?></h1>
  <p><?php print t('Search request failed with error code: %errno', array('%errno' => $exception->getCode())); ?></p>
</div>
