<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['header']: Items for the header region.
 * - $page['navigation']: Items for the navigation region.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>
<!--pre><?php //print_r($page, FALSE); ?></pre-->
<a name="top"></a>
<div id="kopf">
  <div id="kopf_inhalt">
    <?php if (!empty($page['header']['block_1'])): ?>
    <?php print render($page['header']['block_1']); // Logos ?>
    <a href="http://www.schulministerium.nrw.de" target="_blank"><img alt="Logo Bildungsportal Nordrhein-Westfalen" class="logo_mobile" src="/<?php print $directory; ?>/img/bildungsportal_logo_blau_small.png" width="30" height="50" /></a>
    <a href="http://www.schulministerium.nrw.de" target="_blank"><img alt="Medienberatung NRW" class="logo_mobile" src="/<?php print $directory; ?>/img/medienberatung_nrw_logo.svg" width="30" height="50" /></a>
    <?php endif; ?>
    <div class="unsichtbar">
      <h1><?php print $title; ?></h1>
      <?php if (!empty($page['header']['block_2'])): ?>
      <?php print render($page['header']['block_2']); // Orientierungsbereich ?>
      <?php endif; ?>
    </div>
    <div id="hilfsmenu">
      <?php if (!empty($page['header']['menu_block_1'])): ?>
      <?php print render($page['header']['menu_block_1']); // Hilfe Menu ?>
      <?php endif; ?>
    </div>
    <a href="#mobilemenu" id="mmenu-opener" class="fa fa-bars"></a>
    <?php if ($page['header']['menu_block_6']): ?>
    <?php print render($page['header']['menu_block_6']); // Responsives Hauptmenü ?>
    <?php endif; ?>
  </div>
</div>
<div class="main-container container">
  <div class="row-fluid">
    <div id="usermenu">
      <div id="usermenu2">
      <?php if (!empty($page['navigation'])): ?>
        <nav role="navigation" id="siteNavigation">
          <div class="clearfix">
            <?php print render($page['navigation']); // Menu oben ?>
          </div>
        </nav>
      <?php endif; ?>
      </div>
      <div id="outerslide">
        <div id="innerslide">
          <?php if (!empty($page['highlighted'])): ?>
            <?php print render($page['highlighted']); ?>
          <?php endif; ?>
        </div>
      </div>
    </div>
    <div class="mediaselect_wrapper">
      <div class="shadow_left"></div>
      <ul class="mediaselect">
        <?php if (isset($page['wss_search_services']['sodis'])): ?>
          <li<?php if (lls_determine_search_service() === 'sodis') { print ' class="active"'; } ?>>
            <a href="#sodis" data-search-service="sodis"><?php print $page['wss_search_services']['sodis']['title']; ?></a>
          </li>
        <?php endif; ?>
        <?php if (isset($page['wss_search_services']['lfs'])): ?>
          <li<?php if (lls_determine_search_service() === 'lfs') { print ' class="active"'; } ?>>
            <a href="#lfs" data-search-service="lfs"><?php print $page['wss_search_services']['lfs']['title']; ?></a>
          </li>
        <?php endif; ?>
        <?php if (isset($page['wss_search_services']['hbz'])): ?>
          <li<?php if (lls_determine_search_service() === 'hbz') { print ' class="active"'; } ?>>
            <a href="#hbz" data-search-service="hbz"><?php print $page['wss_search_services']['hbz']['title']; ?></a>
          </li>
        <?php endif; ?>
<?php
/**
 * The following block is inactive since we see a way to use the generic solution,
 * honoring the desired order of tabs, without too much refactoring.
 */
/*
        <?php foreach ($page['wss_search_services'] as $search_service_id => $search_service): ?>
        <li<?php if ($search_service_id === lls_determine_search_service()) { print ' class="active"'; } ?>>
          <a href="#<?php print drupal_clean_css_identifier($search_service_id); ?>">
            <?php print $search_service['title']; ?>
          </a>
        </li>
        <?php endforeach; ?>
*/ ?>
      </ul>
      <div class="shadow_right"></div>
    </div>
    <div id="content">
      <div class="well"><?php print $messages; ?></div>
      <?php if (!empty($tabs)): ?>
        <?php //print render($tabs); ?>
      <?php endif; ?>
      <?php if (!empty($page['help'])): ?>
      <div class="well"><?php print render($page['help']); ?></div>
      <?php endif; ?>
      <?php if (!empty($action_links)): ?>
        <ul class="action-links">
          <?php print render($action_links); ?>
        </ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
    </div>
  </div>
</div>
<div id="breadcrumbs">
	<div id="breadcrumbsinner">
    <?php if (!empty($breadcrumb)): ?>
      Sie befinden sich hier: <?php print $breadcrumb; ?>
    <?php endif; ?>
  </div>
</div>
<div id="fuss">
  <div id="fussinnen">
    <?php if (!empty($page['footer']['menu_block_5'])): ?>
    <div class="list">
      <?php print render($page['footer']['menu_block_5']); // Themen im Bildungsportal ?>
    </div>
    <?php endif; ?>
    <?php if (!empty($page['footer']['menu_block_4'])): ?>
    <div class="list extern">
      <?php print render($page['footer']['menu_block_4']); // Organization Menu ?>
    </div>
    <?php endif; ?>
    <div id="info">
      <?php if (!empty($page['footer']['block_4'])): ?>
      <?php print render($page['footer']['block_4']); // Copyright?>
      <?php endif; ?>
    </div>
  </div>
</div>
<div class="hidden">
  <?php print render($page['hidden']); ?>
</div>
