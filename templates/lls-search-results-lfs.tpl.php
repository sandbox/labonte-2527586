<?php
/**
 * @file
 * Template file for displaying the learn:line NRW search box.
 *
 * Variables:
 * - $base_url: Drupal base url.
 * - $module_path: Filesystem path to the module.
 * - $search_term: Current search term.
 * - $active_facets: Facets/Filters used for the current search request.
 * - $available_filters: Applicable search facets/filters.
 * - $total_count: Total number of results.
 * - $count_from: Nummer des ersten angezeigten Treffers (bei fortlaufender
 *     Nummerierung der Treffer von 1 bis X).
 * - $count_to: Nummer des letzten angezeigten Treffers (bei fortlaufender
 *     Nummerierung der Treffer von 1 bis X).
 * - $results: Service response results data array.
 */
$suggested_filters = $available_filters;
?>
<div id="lfs-results">
  <div class="container">
    <div class="row">
      <aside class="col-md-3 filtermenu">
        <?php if ($results): ?>
        <div class="accordion-group">
<!--          <div class="accordion-heading">-->
<!--            <a class="accordion-toggle collapsed" href="#collapseDateFrom" data-toggle="collapse" aria-expanded="false" aria-controls="collapseDateFrom">-->
<!--              Datum-->
<!--            </a>-->
<!--          </div>-->
<!--          <div id="collapseDateFrom" class="accordion-body collapse" aria-expanded="false">-->
<!--            <div class="accordion-inner">-->
<!--              <label>Von:</label>-->
<!--              <input id="ZeitraumVon" type="hidden" value="--><?php //print ($date_from ?: ''); ?><!--" data-url="/--><?php //print $date_from_url; ?><!--"/>-->
<!--              <div class="filter-calendar" data-field="ZeitraumVon"></div>-->
<!--              <label>Bis:</label>-->
<!--              <input id="ZeitraumBis" type="hidden" value="--><?php //print ($date_to ?: ''); ?><!--" data-url="/--><?php //print $date_to_url; ?><!--"/>-->
<!--              <div class="filter-calendar" data-field="ZeitraumBis"></div>-->
<!--              --><?php //if (!empty($suggested_filters['AufAnfrage'])): ?>
<!--              <hr/>-->
<!--              <a href="?--><?php //print $suggested_filters['AufAnfrage']['children'][0]['query']; ?><!--">-->
<!--                Auf Anfrage (--><?php //print $suggested_filters['AufAnfrage']['children'][0]['count']; ?><!--)-->
<!--              </a>-->
<!--              --><?php //unset($suggested_filters['AufAnfrage']); ?>
<!--              --><?php //endif; ?>
<!--            </div>-->
<!--          </div>-->
        </div>
        <?php endif; ?>
        <?php foreach ($suggested_filters as $param => $param_values): ?>
        <div class="accordion-group">
          <div class="accordion-heading">
            <a class="accordion-toggle collapsed" href="#filter-<?php print $param; ?>" data-toggle="collapse" aria-controls="filter-<?php print $param; ?>">
              <?php print $param_values['title']; ?>
            </a>
          </div>
          <div class="accordion-body collapse" id="filter-<?php print $param; ?>">
            <div class="accordion-inner">
              <ul>
                <?php $filter_counter = 0; ?>
                <?php foreach ($param_values['children'] as $value): if ($filter_counter++ > 10) break; ?>
                <li>
                  <a href="?<?php print $value['query']; ?>" data-toggle="tooltip" data-placement="right" title="<?php print $value['value']; ?>">
                    <span class="filter-value"><?php print $value['value']; ?></span>
                    <span class="filter-count">(<?php print $value['count']; ?>)</span>
                  </a>
                </li>
                <?php endforeach; ?>
              </ul>
            </div>
          </div>
        </div>
        <?php endforeach; ?>
        <div>
          <?php print text_summary(lls_get_node_body_by_alias('hinweise/rechtshinweis'), null, 100); ?>
          <a href="/hinweise/rechtshinweis">Weiterlesen</a>
        </div>
      </aside>
      <div class="col-md-9 main-content">
        <div class="searchresult">
          <div class="searchresult-header">
            <div class="row">
              <div class="col-sm-9 col-xs-12">
                <h3>
                  Suchergebnis für:
                  <span class="keyword"><?php print $search_term; ?></span>
                  <small class="result-count">Zeige Treffer <?php print (($results) ? $count_from : 0) . ' - ' . $count_to . ' von <span class="total-count">' . $total_count . '</span>'; ?></small>
                </h3>
              </div>
              <?php if (!empty($active_filters)): ?>
                <div class="col-sm-3 col-xs-12">
                  <a class="filterreset" href="/<?php print request_path(); ?>?primary_service=lfs">
                    Filter zurücksetzen <i class="fa fa-close"></i>
                  </a>
                </div>
              <?php endif; ?>
            </div>
          </div>
          <div class="searchterms location">
<!--            <a class="location-filter" href="#" data-toggle="modal" data-target="#modal-choose-location">--><?php //echo t('Schulstandort'); ?><!-- <span class="llsLocationValue">--><?php //echo t('kein Schulstandort gewählt'); ?><!--</span></a>-->
            <?php foreach ($active_filters as $active_filter): ?>
              <?php if ($active_filter['param'] === 'AufAnfrage'): ?>
                <a href="?<?php print $active_filter['query']; ?>" title="Entferne Filter: Datum (Auf Anfrage)">
                  Termin auf Anfrage
                </a>
                <?php continue; endif; ?>
              <a href="?<?php print $active_filter['query']; ?>" title="Entferne <?php print $active_filter['title'] . ': ' . $active_filter['value']; ?>">
                <?php print $active_filter['value']; ?>
              </a>
            <?php endforeach; ?>
          </div>
        </div>
        <?php if ($results): ?>
        <?php foreach ($results as $result): ?>
          <?php print theme('lls_search_result_lfs', compact('result')); ?>
        <?php endforeach; ?>
        <?php if (!empty($pagination)): ?>
          <?php print render($pagination); ?>
        <?php endif; ?>
        <?php else: ?>
        <?php print lls_get_node_body_by_alias('hinweis/keine-treffer/lfs'); ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
  <div id="filter-menu">
    <div class="filter-title">
      Filter <span class="filter-arrow fa fa-caret-up pull-right"></span>
    </div>
    <div class="filter-content">
      <div class="filter-body">
        <div class="filter-accordion">
<!--          <h3>Datum</h3>-->
<!--          <ul class="filter-selectList">-->
<!--            <li>Zeitraum von:</li>-->
<!--            <li>-->
<!--              <div class="filter-calendar" data-field="ZeitraumVon"></div>-->
<!--            </li>-->
<!--            <li>Zeitraum bis:</li>-->
<!--            <li>-->
<!--              <div class="filter-calendar" data-field="ZeitraumBis"></div>-->
<!--            </li>-->
<!--            --><?php //if (!empty($available_filters['AufAnfrage'])): ?>
<!--            <li>-->
<!--              <a href="?--><?php //print $available_filters['AufAnfrage']['children'][0]['query']; ?><!--">-->
<!--                Nur auf Anfrage (--><?php //print $available_filters['AufAnfrage']['children'][0]['count']; ?><!--)-->
<!--              </a>-->
<!--              --><?php //unset($available_filters['AufAnfrage']); ?>
<!--            </li>-->
<!--            --><?php //endif; ?>
<!--          </ul>-->
          <?php foreach ($available_filters as $param => $param_values): $filter_counter = 0; ?>
          <h3><?php print $param_values['title']; ?></h3>
          <div>
            <ul class="filter-selectList">
              <?php foreach ($param_values['children'] as $value): if ($filter_counter > 10) break; ?>
              <li>
                <a href="?<?php print $value['query']; ?>">
                  <?php print $value['value'] . ' (' . $value['count'] . ')'; ?>
                </a>
              </li>
              <?php endforeach; ?>
            </ul>
          </div>
          <?php endforeach; ?>
        </div>
      </div>
      <div class="filter-footer">
        <div class="col-sm-6 col-sm-offset-3">
          <button class="filter-close">Schließen</button>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
(function($) {
  $(document).ready(function() {
    $('.filter-calendar').each(function (index, calendar) {
      var param = $(calendar).data('field');
      $(calendar).datepicker({
        defaultDate: $('#' + param).val() || null,
        dateFormat: 'dd.mm.yy',
        altField: '#' + param,
        width: 200,
        onSelect: function(dateText, datepicker) {
          var query = location.search.replace(new RegExp(param + '=[^&]*&?'), '')
                                     .replace(/primary_service=[^&]*/, '');
          if (query && query.substr(-1) !== '&') query += '&';
          location.replace((query || '?') + param + '=' + dateText + '&primary_service=lfs');
        }
      });
    });
    $('[data-toggle="tooltip"]').tooltip();

	  /**
	   * Add functionality for reporting dead links.
	   */
    $('.lfs-link.report-dead-link').reportDeadLink({
      confirmText: Drupal.settings.lls.dead_link.confirm_message,
    });

    learnline.loadArticleLinker();
  });
})(jQuery);
</script>
