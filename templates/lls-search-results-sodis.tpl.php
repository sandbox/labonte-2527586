<?php
/**
 * @file
 * Template file for displaying the learn:line NRW search box.
 *
 * Variables:
 * - $base_url: Drupal base url.
 * - $module_path: Filesystem path to the module.
 * - $search_term: Current search term.
 * - $active_facets: Facets/Filters used for the current search request.
 * - $available_facets: Applicable search facets/filters.
 * - $total_count: Total number of results.
 * - $count_from: Nummer des ersten angezeigten Treffers (bei fortlaufender
 *     Nummerierung der Treffer von 1 bis X).
 * - $count_to: Nummer des letzten angezeigten Treffers (bei fortlaufender
 *     Nummerierung der Treffer von 1 bis X).
 * - $results: Service response results data array.
 */

?>

<!-- Modal Medienkompetenz -->
<div class="modal" id="modal-media-competence" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Was bedeutet Medienkompetenz?</h4>
      </div>
      <div class="modal-body">
        <p>Zum besseren Verständnis der verschiedenen Medienkompetenzen haben wir ein PDF erstellt, welches unter folgendem Link heruntergeladen werden kann:</p>
        <p><a href="/sites/default/files/kompetenzrahmen.pdf" target="_blank">PDF Medienkompetenz öffnen</a></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal Medienkompetenz -->

<div id="sodis-results">
  <div class="container">
    <div class="row">
      <aside class="col-md-3 filtermenu">
        <div class="accordion-group">
          <div class="accordion-heading">
            <a class="accordion-toggle collapsed" data-toggle="collapse" href="#collapse7" aria-controls="collapse7" aria-expanded="true">Schulstandort</a>
          </div>
          <div id="collapse7" class="accordion-body collapse" aria-expanded="true">
            <div class="accordion-inner">
              <div class="location">
                <a href="#" data-toggle="modal" data-target="#modal-choose-location"><?php echo t('Schulstandort'); ?><span class="llsLocationFacetValue"><?php print t('kein Schulstandort gewählt'); ?></span></a>
                <p class="ttip">
                  <?php print t('Ihr zuständiges Medienzentrum stellt Ihnen über EDMOND NRW zusätzliche lokal lizensierte Lernmedien bereit. Bitte wählen Sie hierfür Ihren Schulstandort im Kreis bzw. in der kreisfreien Stadt aus.'); ?>
                </p>
              </div>
            </div>
          </div>
        </div>

        <?php foreach ($available_facets as $param => $param_values): ?>
        <div class="accordion-group">
          <div class="accordion-heading">
            <a class="accordion-toggle collapsed" href="#filter-<?php print $param; ?>" data-toggle="collapse" aria-controls="filter-<?php print $param; ?>">
              <?php print $param_values['title']; ?>
            </a>
          </div>
          <div class="accordion-body collapse" id="filter-<?php print $param; ?>">
            <div class="accordion-inner">
              <ul>
                <?php foreach ($param_values['children'] as $value): ?>
                <li>
                  <a href="?<?php print $value['query']; ?>">
                    <?php print $value['value'] . ' (' . $value['count'] . ')'; ?>
                  </a>
                </li>
                <?php endforeach; ?>
              </ul>
            </div>
          </div>
        </div>
        <?php endforeach; ?>
        <div>
          <?php print text_summary(lls_get_node_body_by_alias('hinweise/rechtshinweis'), null, 100); ?>
          <a href="/hinweise/rechtshinweis">Weiterlesen</a>
        </div>
      </aside>
      <div class="col-md-9 main-content">
        <div class="searchresult">

          <div class="searchresult-header">
            <div class="row">
              <div class="col-sm-9 col-xs-12">
                <h3>
                  Suchergebnis für:
                  <span class="keyword"><?php print $search_term; ?></span>
                  <small class="result-count">Zeige Treffer <?php print (($results) ? $count_from : 0) . ' - ' . $count_to . ' von <span class="total-count">' . $total_count . '</span>'; ?></small>
                </h3>
              </div>
              <?php if (!empty($active_facets)): ?>
              <div class="col-sm-3 col-xs-12">
                <a class="filterreset" href="/<?php print request_path(); ?>?primary_service=sodis">
                  Filter zurücksetzen <i class="fa fa-close"></i>
                </a>
              </div>
              <?php endif; ?>
            </div>
          </div>



          <div class="searchterms location">
            <a class="location-filter" href="#" data-toggle="modal" data-target="#modal-choose-location"><?php echo t('Schulstandort'); ?> <span class="llsLocationValue"><?php echo t('kein Schulstandort gewählt'); ?></span></a>
            <?php foreach ($active_facets as $active_facet): ?>
            <a href="?<?php print $active_facet['query']; ?>" title="Entferne <?php print $active_facet['title'] . ': ' . $active_facet['value']; ?>">
              <?php print $active_facet['value']; ?>
            </a>
            <?php endforeach; ?>
          </div>
        </div>
        <?php if ($results): ?>
        <?php foreach ($results as $result): ?>
          <?php print theme('lls_search_result_sodis', compact('result')); ?>
        <?php endforeach; ?>
        <?php if (!empty($pagination)): ?>
          <?php print render($pagination); ?>
        <?php endif; ?>
        <?php else: ?>
        <?php print lls_get_node_body_by_alias('hinweis/keine-treffer/sodis'); ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
  <div id="filter-menu">
    <div class="filter-title">
      Filter <span class="filter-arrow fa fa-caret-up pull-right"></span>
    </div>
    <div class="filter-content">
      <div class="filter-body">
        <div class="filter-accordion">
          <?php foreach ($available_facets as $param => $param_values): ?>
          <h3><?php print $param_values['title']; ?></h3>
          <div>
            <ul class="filter-selectList">
              <?php foreach ($param_values['children'] as $value): ?>
              <li>
                <a href="?<?php print $value['query']; ?>">
                  <?php print $value['value'] . ' (' . $value['count'] . ')'; ?>
                </a>
              </li>
              <?php endforeach; ?>
            </ul>
          </div>
          <?php endforeach; ?>
        </div>
      </div>
      <div class="filter-footer">
        <div class="col-sm-6 col-sm-offset-3">
          <button class="filter-close">Schließen</button>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
(function($) {
  $(document).ready(function() {
    /**
     * Add event listener on result links to assure a location selection for edmond links.
     */
    $('.sodis-title a').on('click', function(event) {
      if (learnline.assureLocationSelection(this)) {
        event.preventDefault();
      }
    });

    /**
     * Add functionality for reporting dead links.
     */
    $('.sodis.report-dead-link').reportDeadLink({
      confirmText: Drupal.settings.lls.dead_link.confirm_message,
    });

	  $('.articlepreview[data-url="' + this.resource + '"]').edutags({
		  domain: Drupal.settings.lls.edutags.domain,
		  base_url: Drupal.settings.lls.edutags.base_url,
		  data: this
	  });

    /**
     * Replace unavailable thumbnail images.
     */
    $('#sodis-results').find('img.remote-src').each(function() {
      $(this).remoteImage();
    });

    learnline.loadArticleLinker();

    /**
     * Edutags widgets initialization.
     */
    if (Drupal.settings.lls.edutags.active) {
      /**
       * Fetch resource URLs for edutags request.
       */
      var urls = [];
      $('.articlepreview.sodis').each(function() {
        if ($(this).data('url').search(/^http[s]?:\/\//i) === 0) {
          urls.push($(this).data('url'));
        }
      });

      if (urls.length > 0) {
        $.ajax({
          url: Drupal.settings.basePath + 'lls/edutags',
          method: 'GET',
          dataType: 'json',
          data: { urls: urls },
          // Initialize edutags widgets with data:
          success: function(data) {
            $(data).each(function () {
              urls.splice(urls.indexOf(this.resource), 1);
              // Find matching element based on resource URL and initialize edutags widget.
              $('.articlepreview[data-url="' + this.resource + '"]').edutags({
                domain: Drupal.settings.lls.edutags.domain,
                base_url: Drupal.settings.lls.edutags.base_url,
                data: this
              });
            });
          },
          // Initialize edutags widgets without any data:
          complete: function () {
            $(urls).each(function() {
              $('.articlepreview[data-url="' + this + '"]').edutags({
                domain: Drupal.settings.lls.edutags.domain,
                base_url: Drupal.settings.lls.edutags.base_url,
                data: { tags: [], comments: [], rating_stars: 0 }
              });
            });
          }
        });
      }
    }
  });
})(jQuery);
</script>
