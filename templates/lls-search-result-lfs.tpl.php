<?php
/**
 * @file
 * Template for a single SODIS result/hit.
 */
use \publicplan\lls\LfsHelper;
use \publicplan\lls\WssHelper;

// Clean result ID for use as HTML id attribute:
$htmlid = 'lfs-' . drupal_clean_css_identifier($result['id']);

// Determine a fallback thumbnail. This will be used, in two cases:
//  1. There is no thumbnail specified.
//  2. We detect (via JS), that the specified thumbnail cannot be loaded.
$theme_dir = drupal_get_path('theme', 'learnline4');

// Whitelist all attributes to show in the collapsed 'more' area
// 'Anfangstermin' => 'Anfangstermin',
// 'Endtermin' => 'Endtermin',
// 'Uhrzeit' => 'Uhrzeit',
// 'AnbieterId' => 'AnbieterId',
// 'Kursnummer' => 'Kursnummer',
// 'Veranstaltungsort' => 'Veranstaltungsort',
// 'AufAnfrage' => 'Auf Anfrage',
// 'Zielgruppen' => 'Zielgruppen',
// 'Veranstaltungshinweise' => 'Veranstaltungshinweise',
// 'Dozent' => 'Dozent',
// 'Kontaktemail' => 'Kontaktemail',
// 'Homepage' => 'Homepage',
// 'Gesamtpreis' => 'Gesamtpreis',
$more_display = array(
  array(
    'Berufsfelder' => array(
      'display_name' => 'Fächer',
      'filter_name' => 'disciplines',
      'is_attribute' => false,
    ),
    'Schulformen' => array(
      'display_name' => 'Schulformen',
      'is_attribute' => true,
    ),
  ),
  array(
    'Traeger' => array(
      'display_name' => 'Träger',
      'is_attribute' => false,
    ),
    'Veranstalter' => array(
      'display_name' => 'Veranstalter',
      'is_attribute' => false,
    ),
    'Veranstaltungsform' => array(
      'display_name' => 'Veranstaltungsform',
      'is_attribute' => false,
    ),
  ),
  array(
    'Kategorie' => array(
      'display_name' => 'Kategorie',
      'is_attribute' => false,
    ),
    'Rubrik' => array(
      'display_name' => 'Rubrik',
      'is_attribute' => true,
    ),

  ),
);
?>
<div class="articlepreview lfs" id="<?php print $htmlid; ?>" data-id="<?php print $result['id']; ?>" data-url="<?php print empty($result['lfs_url']) ? '' : $result['lfs_url']; ?>" data-service="lfs">
  <div class="row">
    <div class="col-md-12">
      <div class="row">
		  <div class="col-md-12 links">
			  <a class="lfs-link report-dead-link" href="" onclick="return false;"><?php print t('Defekten Link melden'); ?></a> <a href="" class="share-link" onclick="return false;"><?php print t('Teilen'); ?></a> <a href="" class="directlink-link" onclick="return false;"><?php print t('Direktlink'); ?></a>
		  </div>
      </div>

		<div class="row share">
			<div class="col-md-12">
				<div class="shariff" data-url="<?php print empty($result['lfs_url']) ? '' : $result['lfs_url']; ?>"></div>
			</div>
		</div>

		<div class="row directlink">
			<div class="col-md-12">
				<div class="directlink-container"><input type="text" readonly="readonly" value="<?php print empty($result['lfs_url']) ? '' : $result['lfs_url']; ?>"/></div>
			</div>
		</div>
      <div class="row">
        <div class="col-md-12">
          <h3><?php print $result['Veranstalter']; ?></h3>
          <h2 class="lfs-title">
            <?php if (!empty($result['lfs_url'])): ?>
            <a href="<?php print $result['lfs_url']; ?>" target="_blank">
              <i class="fa fa-external-link"></i>
              <?php print $result['Titel']; ?>
            </a>
            <?php else: ?>
            <?php print $result['Titel']; ?>
            <?php endif; ?>
          </h2>
          <div class="metainfo">
            <?php if (isset($result['Veranstaltungsort']) && !empty($result['Veranstaltungsort'])): ?>
              <i class="fa fa-map-marker"></i>
              <span><?php print $result['Veranstaltungsort']; ?></span>
            <?php endif; ?>
            <?php if (isset($result['Datum']) && !empty($result['Datum'])): ?>
              <i class="fa fa-calendar"></i>
              <span><?php print $result['Datum']; ?></span>
              <?php if (isset($result['Uhrzeit']) && !empty($result['Uhrzeit'])): ?>
                <i class="fa fa-clock-o"></i>
                <span><?php print $result['Uhrzeit']; ?></span>
              <?php endif; ?>
            <?php endif; ?>
            <?php if (isset($result['AufAnfrage']) && $result['AufAnfrage']): ?>
              <i class="fa fa-calendar-times-o"></i>
              <span><?php print t('Termin auf Anfrage'); ?></span>
            <?php endif; ?>
          </div>
          <p class="lfs-description"><?php print lls_text_summary($result['Beschreibung'], 500); ?></p>
          <?php if (!empty($result['Kontaktemail'])): ?>
            <p>
              <a href="mailto:<?php print $result['Kontaktemail']; ?>" target="_blank">
                <i class="fa fa-envelope"></i>
                <?php print $result['Kontaktemail']; ?>
              </a>
            </p>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 expand">
      <a class="btn btn-primary SeeMore collapsed" role="button" href="#more-<?php print $htmlid; ?>" data-toggle="collapse" aria-expanded="false" aria-controls="collapseMore"><?php print t('Mehr'); ?></a>
      <div id="more-<?php print $htmlid; ?>" class="collapse">
        <div class="accordioncontent">
          <div class="row">
            <?php print WssHelper::getMoreFilters($result, $more_display); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
