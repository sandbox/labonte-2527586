<?php
/**
 * @file
 * learn:line NRW outer HTML template.
 *
 * Variables:
 * - $css: An array of CSS files for the current page.
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or 'rtl'.
 * - $rdf_namespaces: All the RDF namespace prefixes used in the HTML document.
 * - $grddl_profile: A GRDDL profile allowing agents to extract the RDF data.
 * - $head_title: A modified version of the page title, for use in the TITLE
 *   tag.
 * - $head_title_array: (array) An associative array containing the string parts
 *   that were used to generate the $head_title variable, already prepared to be
 *   output as TITLE tag. The key/value pairs may contain one or more of the
 *   following, depending on conditions:
 *   - title: The title of the current page, if any.
 *   - name: The name of the site.
 *   - slogan: The slogan of the site, if any, and if there is no title.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $page_top: Initial markup from any modules that have altered the
 *   page. This variable should always be output first, before all other dynamic
 *   content.
 * - $page: The rendered page content.
 * - $page_bottom: Final closing markup from any modules that have altered the
 *   page. This variable should always be output last, after all other dynamic
 *   content.
 * - $classes String of classes that can be used to style contextually through
 *   CSS.
 * - $meta_description: Value for the appropriate HTML meta tag.
 * - $meta_keywords: Value for the appropriate HTML meta tag.
 * - $meta_author: Value for the appropriate HTML meta tag.
 * - $meta_copyright: Value for the appropriate HTML meta tag.
 * - $meta_robots: Value for the appropriate HTML meta tag.
 */
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0, minimum-scale=1.0">
    <?php print $head; ?>
    <meta name="keywords" content="<?php print $meta_description; ?>">
    <meta name="description" content="<?php print $meta_keywords; ?>">
    <meta name="author" content="<?php print $meta_author; ?>">
    <meta name="copyright" content="<?php print $meta_copyright; ?>">
    <meta name="robots" content="<?php print $meta_robots; ?>">
    <?php print $styles; ?>
    <?php if (drupal_is_front_page()): ?>
    <?php endif; ?>
    <?php print $scripts; ?>
    <script>jwplayer.key="dEFnCTgSH22vLdwhdaCyOSF0Lz0yzylnE72v0Q==";</script>
    <link rel="alternate" type="application/rss+xml" href="http://www.schulministerium.nrw.de/docs/bp/Ministerium/Service/RSS/_presse.xml" title="Pressemitteilungen">
    <!-- favicons -->
    <link rel="shortcut icon" href="/sites/all/themes/learnline4/favicon.ico" type="image/vnd.microsoft.icon"/>
    <link href="/sites/all/themes/learnline4/favicon.ico" rel="icon" title="Icon"/>
<!--    <link rel="apple-touch-icon" sizes="57x57" href="/--><?php //print $directory; ?><!--/img/favicons/apple-touch-icon-57x57.png">-->
<!--    <link rel="apple-touch-icon" sizes="60x60" href="/--><?php //print $directory; ?><!--/img/favicons/apple-touch-icon-60x60.png">-->
<!--    <link rel="apple-touch-icon" sizes="72x72" href="/--><?php //print $directory; ?><!--/img/favicons/apple-touch-icon-72x72.png">-->
<!--    <link rel="apple-touch-icon" sizes="76x76" href="/--><?php //print $directory; ?><!--/img/favicons/apple-touch-icon-76x76.png">-->
<!--    <link rel="apple-touch-icon" sizes="114x114" href="/--><?php //print $directory; ?><!--/img/favicons/apple-touch-icon-114x114.png">-->
<!--    <link rel="apple-touch-icon" sizes="120x120" href="/--><?php //print $directory; ?><!--/img/favicons/apple-touch-icon-120x120.png">-->
<!--    <link rel="apple-touch-icon" sizes="144x144" href="/--><?php //print $directory; ?><!--/img/favicons/apple-touch-icon-144x144.png">-->
<!--    <link rel="apple-touch-icon" sizes="152x152" href="/--><?php //print $directory; ?><!--/img/favicons/apple-touch-icon-152x152.png">-->
<!--    <link rel="apple-touch-icon" sizes="180x180" href="/--><?php //print $directory; ?><!--/img/favicons/apple-touch-icon-180x180.png">-->
<!--    <link rel="icon" type="image/png" href="/--><?php //print $directory; ?><!--/img/favicons/favicon-32x32.png" sizes="32x32">-->
<!--    <link rel="icon" type="image/png" href="/--><?php //print $directory; ?><!--/img/favicons/android-chrome-192x192.png" sizes="192x192">-->
<!--    <link rel="icon" type="image/png" href="/--><?php //print $directory; ?><!--/img/favicons/favicon-96x96.png" sizes="96x96">-->
<!--    <link rel="icon" type="image/png" href="/--><?php //print $directory; ?><!--/img/favicons/favicon-16x16.png" sizes="16x16">-->
<!--    <link rel="manifest" href="/--><?php //print $directory; ?><!--/img/favicons/manifest.json">-->
<!--    <link rel="mask-icon" href="/--><?php //print $directory; ?><!--/img/favicons/safari-pinned-tab.svg" color="#5bbad5">-->
<!--    <meta name="msapplication-TileColor" content="#2d89ef">-->
<!--    <meta name="msapplication-TileImage" content="/mstile-144x144.png">-->
<!--    <meta name="theme-color" content="#ffffff">-->
    <!-- favicons -->
    <title><?php print $head_title; ?></title>
    <script type="text/javascript" src="https://idp.blau.logineo.de/navigation/navbar_base.js"></script>
    <?php if (!empty($loSchoolId)): ?>
      <script type="text/javascript" src="https://idp.blau.logineo.de/navigation/navbar_config.js?schoolId=<?php echo $loSchoolId ?>&currentService=<?php echo $loAppId ?>"></script>
    <?php endif; ?>
    <!-- twemoji -->
    <script src="//twemoji.maxcdn.com/2/twemoji.min.js?2.3.0"></script>
    <!-- /twemoji -->
  </head>
  <body class="<?php print $classes; ?>" <?php print $attributes;?>>
    <div id="page-overlay" style="background-image:url('/<?php print drupal_get_path('theme', 'learnline4'); ?>/img/ajax-loader.gif');">
      <div id="page-overlay-wrapper">
        <div id="page-overlay-head">
          <span id="page-overlay-close" class="highlight">
            <?php print t('close'); ?>
            <i class="fa fa-times-circle-o"></i>
          </span>
        </div>
        <div id="page-overlay-body"></div>
      </div>
    </div>
    <?php print $page_top; ?>
    <div id="rahmen">
      <?php print $page; ?>
    </div>
    <?php print $page_bottom; ?>
  </body>

  <!-- Document Initialize -->
  <script type="text/javascript">
    jQuery(document).ready(function () {
      var filterMen = new filterMenu();
      filterMen.init();
      var menu = new mobileMenu();
      menu.init(function(){filterMen.hide();}, function(){filterMen.show();});
    });
  </script>
  <!-- /Document Initialize -->

  <!-- datepicker -->
  <script>
    jQuery(function ($) {
      $.datepicker.regional['de'] = {clearText: 'löschen', clearStatus: 'aktuelles Datum löschen',
        closeText: 'schließen', closeStatus: 'ohne Änderungen schließen',
        prevText: '<zurück', prevStatus: 'letzten Monat zeigen',
        nextText: 'Vor>', nextStatus: 'nächsten Monat zeigen',
        currentText: 'heute', currentStatus: '',
        monthNames: ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni',
          'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'],
        monthNamesShort: ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun',
          'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
        monthStatus: 'anderen Monat anzeigen', yearStatus: 'anderes Jahr anzeigen',
        weekHeader: 'Wo', weekStatus: 'Woche des Monats',
        dayNames: ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'],
        dayNamesShort: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
        dayNamesMin: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
        dayStatus: 'Setze DD als ersten Wochentag', dateStatus: 'Wähle D, M d',
        dateFormat: 'dd.mm.yy', firstDay: 1,
        initStatus: 'Wähle ein Datum', isRTL: false};
      $.datepicker.setDefaults($.datepicker.regional['de']);
    });
  </script>
  <!-- /datepicker -->

  <!-- twemoji -->
  <script>
    jQuery(document).ready(function() {
      var twitterFeed = document.getElementById('twitter-inner');
      if (twitterFeed) {
        twemoji.parse(twitterFeed);
      }
    });
  </script>
  <!-- /twemoji -->
</html>
