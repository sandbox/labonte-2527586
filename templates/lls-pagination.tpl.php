<?php
/**
 * @file
 * Pagination template.
 *
 * Variables:
 * - $element: The pagination as nested array.
 */
?>
<?php if (!empty($element['pages'])): ?>
<div class="pagination">
  <ul>
    <?php if (!empty($element['first'])): ?>
    <li>
      <a href="<?php print _lls_pagination_query($element['first']); ?>" title="<?php print t('First (page @page)', array('@page' => $element['first'])); ?>">
        <i class="fa fa-angle-double-left"></i>
      </a>
    </li>
    <?php endif; ?>
    <?php if (!empty($element['previous'])): ?>
    <li>
      <a href="<?php print _lls_pagination_query($element['previous']); ?>" title="<?php print t('Previous (page @page)', array('@page' => $element['previous'])); ?>">
        <i class="fa fa-angle-left"></i>
      </a>
    </li>
    <?php endif; ?>
    <?php foreach ($element['pages'] as $page_no => $current_page): ?>
      <?php if ($current_page): ?>
      <li class="active"><?php print $page_no; ?></li>
      <?php else: ?>
      <li><a href="<?php print _lls_pagination_query($page_no); ?>"><?php print $page_no; ?></a></li>
      <?php endif; ?>
    <?php endforeach; ?>
    <?php if (!empty($element['next'])): ?>
    <li>
      <a href="<?php print _lls_pagination_query($element['next']); ?>" title="<?php print t('Next (page @page)', array('@page' => $element['next'])); ?>">
        <i class="fa fa-angle-right"></i>
      </a>
    </li>
    <?php endif; ?>
    <?php if (!empty($element['last'])): ?>
    <li>
      <a href="<?php print _lls_pagination_query($element['last']); ?>" title="<?php print t('Last (page @page)', array('@page' => $element['last'])); ?>">
        <i class="fa fa-angle-double-right"></i>
      </a>
    </li>
    <?php endif; ?>
  </ul>
</div>
<?php endif; ?>
