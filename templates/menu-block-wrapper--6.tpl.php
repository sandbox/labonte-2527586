<?php
/**
 * @file
 * Template for the responsive main menu block.
 *
 * Variables:
 * - $content: The actual render array.
 * - $config: Various meta data about the current menu block.
 * - $delta: The menu_block ID.
 */
$first_level = array();
$second_levels = array();
$theme_path = drupal_get_path('theme', 'learnline4');

foreach ($content as $item) {
  if (empty($item['#title']) || empty($item['#href'])) {
    continue;
  }
  $id = drupal_clean_css_identifier($item['#title']);
  if (!empty($item['#below'])) {
    $second_levels[$id] = $item['#below'];
  }
  $first_level[$id] = $item;
}
?>
<nav id="mobilemenu">
  <div id="mm-slick-slider">
    <?php foreach ($first_level as $item_id => $item): ?>
      <div data-menu-id="<?php print $item_id; ?>" data-title="<?php print $item['#title']; ?>">
        <a href="#" title="<?php print $item['#title']; ?>">
          <img src="/<?php print $theme_path; ?>/img/icon-<?php print $item_id; ?>.svg"/>
        </a>
      </div>
    <?php endforeach; ?>
  </div>
  <ul>
    <?php foreach ($second_levels as $menu_id => $submenu): ?>
      <?php foreach ($submenu as $item): ?>
        <?php if (!empty($item['#title']) && !empty($item['#href'])): ?>
          <li data-menu-parent-id="<?php print $menu_id; ?>">
            <a href="<?php echo url($item['#original_link']['link_path'], $item['#original_link']['options']); ?>">
              <?php print $item['#title']; ?>
            </a>
          </li>
        <?php endif; ?>
      <?php endforeach; ?>
    <?php endforeach; ?>
  </ul>
</nav>
