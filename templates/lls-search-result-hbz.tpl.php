<?php
/**
 * @file
 * Template for a single HBZ result/hit.
 */
// Clean result ID for use as HTML id attribute:
$htmlid = drupal_clean_css_identifier($result['title']);
$thumb_src = $base_url . '/' . $theme_dir;
if (isset($result['href'])) {
  $thumb_src .= '/img/Medientypen_Website.png';
} elseif (isset($result['isbn'])) {
  $thumb_src .= '/img/Medientypen_Text.png';
} else {
  $thumb_src .= '/img/Medientypen_Anderer_Ressourcentyp.png';
}
$keywords = explode(';', $result['keywords']);
?>
<div class="articlepreview hbz" id="<?php print $htmlid; ?>" data-id="<?php $result['title']; ?>"
     <?php if (!empty($result['href'])) { print 'data-url="' . $result['href'] . '"'; } ?>>
  <div class="row">
    <div class="col-md-2 col-xs-3">
      <div class="thumbnailcontainer">
        <img class="img-responsive" src="<?php print $thumb_src; ?>"/>
      </div>
<?php /*      <p class="ressource_type"><?php print $result['learningResourceType'] ?: 'Anderer Ressourcentyp'; ?></p> */?>
    </div>
    <div class="col-md-10 col-xs-9">
      <div class="row">
        <div class="col-md-12 links">
          <div class="col-md-4 col-md-offset-8">
            <?php if (!empty($result['href'])): ?>
<!--            <a href="#">Defekten Link melden</a>-->
            <?php endif; ?>
          </div>
<?php /*          <div class="col-md-4 edutags">
            <span class="rating">
              <i class="fa fa-star empty"></i>
              <i class="fa fa-star empty"></i>
              <i class="fa fa-star empty"></i>
              <i class="fa fa-star empty"></i>
              <i class="fa fa-star empty"></i>
            </span>
            <span class="tags">&nbsp;</span>
            <span class="comments">&nbsp;</span>
          </div>
 */ ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <h3><?php print $result['author']; ?></h3>
          <h2 class="hbz-title">
            <?php if (!empty($result['href'])): ?>
            <a href="<?php print $result['href']; ?>" target="_blank"><?php print $result['title']; ?></a>
            <?php else: ?>
            <?php print $result['title']; ?>
            <?php endif; ?>
          </h2>
          <p class="hbz-description">
            <?php print $result['keywords']; ?>
          </p>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 expand">
      <a class="btn btn-primary SeeMore collapsed" role="button" href="#more-<?php print $htmlid; ?>" data-toggle="collapse" aria-expanded="false" aria-controls="collapseMore">Mehr</a>
      <div id="more-<?php print $htmlid; ?>" class="collapse">
        <div class="accordioncontent">
          <div class="row">
            <?php if (!empty($keywords)): ?>
            <div class="col-md-6 col-xs-12">
              <h4>Schlüsselwörter</h4>
              <?php foreach ($keywords as $keyword): ?>
                <span><?php print $keyword; ?></span>
              <?php endforeach; ?>
            </div>
            <?php endif; ?>
            <?php if (!empty($result['isbn'])): ?>
              <div class="col-md-6 col-xs-12">
                <h4>ISBN</h4>
                <span><?php print $result['isbn']; ?></span>
              </div>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
