<?php
/**
 * @file
 * Blog tags view template to display a list of rows.
 *
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 *
 */
?>
<?php print $wrapper_prefix; ?>
  <?php print $list_type_prefix; ?>
    <?php foreach ($rows as $id => $row): ?>
      <li class="<?php print $classes_array[$id]; ?>"><?php print $row; ?></li>
    <?php endforeach; ?>
  <?php print $list_type_suffix; ?>
<?php print $wrapper_suffix; ?>