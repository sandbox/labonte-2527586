<?php
// $id:$

/**
 * @file
 * Template file for displaying the most recent learn:line NRW resources.
 */
$module_path = drupal_get_path('module', 'lls');
$query_url = '/suche/'.(isset($element['#block']->term) ? $element['#block']->term : '**');
if (isset($element['#block']->query)) {
  unset($element['#block']->query['size']);
  $query_url .= '?' . http_build_query($element['#block']->query);
}
?>
<h2>
  <span class="headline">
    <?php print $element['#block']->subject; ?>
  </span>
</h2>
<div id="lls-most-recent-container">
  <div id="lls-most-recent-loading">
    <img src="<?php print '/' . $module_path . '/img/ajax-loader4.gif'; ?>" alt="Loading content..."/>
  </div>
  <div id="lls-most-recent-content" style="display: none;"></div>
</div>
<p><a class="boxlinkbutton" href="<?php echo $query_url; ?>">mehr ...</a></p>
