<?php
/**
 * @file
 * Template file for displaying the learn:line NRW search box.
 *
 * Variables:
 * - $base_url: Drupal base url.
 * - $module_path: Filesystem path to the module.
 * - $search_term: Current search term.
 * - $active_facets: Facets/Filters used for the current search request.
 * - $available_facets: Applicable search facets/filters.
 * - $total_count: Total number of results.
 * - $count_from: Nummer des ersten angezeigten Treffers (bei fortlaufender
 *     Nummerierung der Treffer von 1 bis X).
 * - $count_to: Nummer des letzten angezeigten Treffers (bei fortlaufender
 *     Nummerierung der Treffer von 1 bis X).
 * - $results: Service response results data array.
 */
?>
<div id="hbz-results">
  <div class="container">
    <div class="row">
      <aside class="col-md-3 filtermenu">
        <?php foreach ($available_facets as $param => $param_values): ?>
        <div class="accordion-group">
          <div class="accordion-heading">
            <a class="accordion-toggle collapsed" href="#filter-<?php print $param; ?>" data-toggle="collapse" aria-controls="filter-<?php print $param; ?>">
              <?php print $param_values['title']; ?>
            </a>
          </div>
          <div class="accordion-body collapse" id="filter-<?php print $param; ?>">
            <div class="accordion-inner">
              <ul>
                <?php foreach ($param_values['children'] as $value): ?>
                <li>
                  <a href="?<?php print $value['query']; ?>">
                    <?php print $value['value'] . ' (' . $value['count'] . ')'; ?>
                  </a>
                </li>
                <?php endforeach; ?>
              </ul>
            </div>
          </div>
        </div>
        <?php endforeach; ?>
      </aside>
      <div class="col-md-9 main-content">
        <div class="searchresult">
          <div class="searchresult-header">
            <div class="row">
              <div class="col-sm-9 col-xs-12">
                <h3>
                  Suchergebnis für:
                  <span class="keyword"><?php print $search_term; ?></span>
                  <?php if ($total_count): ?>
                  <small class="result-count">Zeige Treffer <?php print $count_from . ' - ' . $count_to . ' von <span class="total-count">' . $total_count . '</span>'; ?></small>
                  <?php endif; ?>
                </h3>
              </div>
              <?php if (!empty($active_facets)): ?>
              <div class="col-sm-3 col-xs-12">
                <a class="filterreset" href="/<?php print request_path(); ?>">
                  Filter zurücksetzen <i class="fa fa-close"></i>
                </a>
              </div>
              <?php endif; ?>
            </div>
          </div>
          <div class="searchterms">
            <?php foreach ($active_facets as $active_facet): ?>
            <a href="?<?php print $active_facet['query']; ?>" title="Entferne <?php print $active_facet['title'] . ': ' . $active_facet['value']; ?>">
              <?php print $active_facet['value']; ?>
            </a>
            <?php endforeach; ?>
          </div>
        </div>
        <?php foreach ($results as $result): ?>
          <?php print theme('lls_search_result_hbz', compact('result')); ?>
        <?php endforeach; ?>
        <?php if (!empty($pagination)): ?>
          <?php print render($pagination); ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
  <div id="filter-menu">
    <div class="filter-title">
      Filter <span class="filter-arrow fa fa-caret-up pull-right"></span>
    </div>
    <div class="filter-content">
      <div class="filter-body">
        <div class="filter-accordion">
          <?php foreach ($available_facets as $param => $param_values): ?>
          <h3><?php print $param_values['title']; ?></h3>
          <div>
            <ul class="filter-selectList">
              <?php foreach ($param_values['children'] as $value): ?>
              <li>
                <a href="?<?php print $value['query']; ?>">
                  <?php print $value['value'] . ' (' . $value['count'] . ')'; ?>
                </a>
              </li>
              <?php endforeach; ?>
            </ul>
          </div>
          <?php endforeach; ?>
        </div>
      </div>
      <div class="filter-footer">
        <div class="col-sm-6 col-sm-offset-3">
          <button class="filter-close">Schließen</button>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
(function($) {
  $(document).ready(function() {
    $('#hbz-results').find('img.remote-src').each(function() {
      $(this).remoteImage();
    });
  });
})(jQuery);
</script>
