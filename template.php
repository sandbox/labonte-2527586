<?php

use publicplan\lls\SimpleSAMLphpWrapper as SimpleSAML;


/**
 * @file template.php
 */

/**
 * Implements hook_page_alter().
 */
function _render_front_page($content) {

  /*
  $nids = array(128, 132, 136);
  $nodes = node_load_multiple($nids);
  $temp = node_view_multiple($nodes, 'full');
  foreach ($temp['nodes'] as $key => $value) {
    $content['system_main']['nodes'][$key] = $value;
  }
  */

  //$content['block_9']['#markup'] = $content['block_10']['#markup'];

  return render($content);
}

/**
 * Returns the correct span class for a region
 */
function _learnline4_content_span($columns = 1) {
  $class = FALSE;
  switch($columns) {
    case 1:
      $class = 'span12';
      break;
    case 2:
      $class = 'span9';
      break;
    case 3:
      $class = 'span6';
      break;
  }
  return $class;
}

/**
 * Override theme_breadrumb().
 *
 * Print breadcrumbs as a list, with separators.
 */
function learnline4_breadcrumb($variables) {
  if (count($variables['breadcrumb']) <= 0) {
    return '<strong>' . t('Home') . '</strong>';
  }
  $sep = ' &gt; ';
  $output = '';
  $title = drupal_get_title();
  foreach ($variables['breadcrumb'] as $key => $value) {
    // reset output buffer if it contains appropriate string.
    if (strpos($value, 'learn:line NRW') !== FALSE) {
      $output = $value . $sep;
    } else {
      // title ends with text of value link?
      $text = strip_tags($value);
      if (substr($title, -strlen($text)) === $text) {
        // yes, then truncate,
        $title = trim(substr($title, 0 , strlen($title)-strlen($text)));
      } else {
        // fix bug in wp_blog ?
        $temp = t($text);
        if (substr($title, -strlen($temp)) === $temp) {
          $title = trim(substr($title, 0 , strlen($title)-strlen($temp)));
          $value = str_replace($text, $temp, $value);
        }
      }
      $output .= $value . $sep;
    }
  }
  return $output . '<strong>' . $title . '</strong>';
}

/**
 * Menu block uses Drupal core's menu theme functions. However, it also provides
 * theme hook suggestions that can be used to override any of the theme functions
 * called by it.
 *
 * - theme_menu_tree() can be overridden by creating one of:
 *   - [theme]_menu_tree__[menu name]()
 *   - [theme]_menu_tree__menu_block()
 *   - [theme]_menu_tree__menu_block__[menu name]()
 *   - [theme]_menu_tree__menu_block__[block id number]()
 *
 * - theme_menu_link() can be overridden by creating one of:
 *   - [theme]_menu_link__[menu name]()
 *   - [theme]_menu_link__menu_block()
 *   - [theme]_menu_link__menu_block__[menu name]()
 *
 */

/**
 * Returns HTML for a single main menu link in the navigation area.
 */
function learnline4_menu_link__menu_block__2($data) {
  $el = $data['element'];

  if (empty($el['#original_link']['depth'])) {
    return "\n" . '<li><a href="/">(none)</a></li>'; // ????
  }

  if ($key = array_search('active-trail' , $el['#attributes']['class'])) {
    $el['#attributes']['class'][$key] = 'auswahl';
  }

  if ($el['#original_link']['depth'] == 1) {
    $match = array();
    if (preg_match('/^http[s]?:\/\/?:(www\.)?learnline|^(?:(?!http))/i', $el['#href'],$match)) {
      $el['#attributes']['class'][] = 'web-auswahl';
      $el['#attributes']['class'][] = 'web-auswahl-liste';
    } else {
      $el['#title'] = '<span class="raquo">&raquo;</span> ' . $el['#title'];
      $el['##attributes']['class'][] = 'nav_ext';
      $el['#localized_options']['html'] = TRUE;
    }
    $link = '<a class="titel" href="' . $el['#href'] . '">' . $el['#title'] . '</a>';
  } else {
    $link = l($el['#title'], $el['#href'], $el['#localized_options']);
  }

  $attr = drupal_attributes($el['#attributes']);
  $sub_menu = drupal_render($el['#below']);
  return sprintf("\n<li %s>%s %s</li>", $attr, $link, $sub_menu);
}

/**
 * Returns HTML for a single main menu link in the left sidebar.
 */
function learnline4_menu_link__menu_block__3($data) {
  return learnline4_menu_link__menu_block__2($data);
}

/**
 * Returns HTML for a single main menu link in the footer area.
 */
function learnline4_menu_link__menu_block__5($data) {
  $el = $data['element'];
  // render any classes or other attributes that need to go in this <li>.
  if ($el['#original_link']['depth'] == 1) {
    $el['#attributes']['class'][] = 'footerlistentry';
  }
  if (($key = array_search('active-trail' , $el['#attributes']['class'])) !== FALSE) {
    // follow active trail replacing style class.
    $el['#attributes']['class'][$key] = 'auswahl';
  }
  $attr = drupal_attributes($el['#attributes']);
  // render the menu link.
  $link = l($el['#title'], $el['#href'], $el['#localized_options']);
  // render any submenus
  $sub_menu = drupal_render($el['#below']);
  if (!empty($sub_menu)) {
    $sub_menu = '<div><ul class="footerlistentrysub">' . $sub_menu . '</ul></div>';
  }
  return sprintf("\n<li %s>%s %s</li>", $attr, $link, $sub_menu);
}

/**
 * Returns HTML for a single support navigation menu link.
 */
function learnline4_menu_link__menu_block__menu_support_navigation($data) {
  global $base_url;
  $el = $data['element'];
  // check menu item custom attribute.
//  if (isset($el['#localized_options']['attributes']['rel'])) {
//    // Relationship is used as image in the /sites/default/files/images/ directory.
//    $img = '<img src="/sites/default/files/images/' . $el['#localized_options']['attributes']['rel'] . '" />';
//    unset($el['#localized_options']['attributes']['rel']);
//    $el['#localized_options']['html'] = TRUE;
//    $link = l($img . str_replace('_', '', $el['#title']),
//      ($el['#href']=='<front>')? (drupal_is_front_page()? substr($_SERVER['REQUEST_URI'], 1) : $_GET['q']) : $el['#href'],
//        $el['#localized_options']);
//    // clear leaf class for styling reasons.
//    if (($key = array_search('leaf' , $el['#attributes']['class'])) !== FALSE) {
//      unset($el['#attributes']['class'][$key]);
//    }
//    $attr = drupal_attributes($el['#attributes']);
//    return sprintf("\n<li %s>%s</li>", $attr, $link);
//  }
  if (isset($el['#localized_options']['attributes']['rel'])) {
    // Relationship is used as CSS class for the <i>-tag containing the link symbol.
    $i = '<i class="' . $el['#localized_options']['attributes']['rel'] . '"></i>&nbsp;';
    unset($el['#localized_options']['attributes']['rel']);
    $el['#localized_options']['html'] = TRUE;
    $link_text = $i . str_replace('_', '', $el['#title']);

    // Link menu item.
    if (isset($el['#localized_options']['attributes']['id']) &&
        $el['#localized_options']['attributes']['id'] === 'print-button') {
      $link = '<a href="javascript:window.print();">' . $link_text . '</a>';
    }
    else {
      $link = l($link_text,
          $el['#href'] === '<front>' ? $base_url . '/' : $el['#href'],
          $el['#localized_options']);
    }

    // Clear leaf class for styling reasons.
    if (($key = array_search('leaf' , $el['#attributes']['class'])) !== FALSE) {
      unset($el['#attributes']['class'][$key]);
    }

//    // Hide Logineo SignOn Button if user already logined
//    if (($el['#href'] == 'logineo/sso') && (SimpleSAML::getInstance()->isAuthenticated())) {
//      $el['#attributes']['class'][] = 'invisible';
//    }

    $attr = drupal_attributes($el['#attributes']);
    return sprintf("\n<li %s>%s</li>", $attr, $link);
  }
  return theme_menu_link($data);
}
/**
 * Returns HTML for a single organization menu link.
 */
function learnline4_menu_link__menu_block__menu_organization_menu($data) {
  $el = $data['element'];
  // check menu item custom attribute.
  if (isset($el['#localized_options']['attributes']['rel'])) {
    // Relationship is used as image in the /sites/default/files/images/logos/ directory.
    $img = '<img src="/sites/default/files/images/logos/' . $el['#localized_options']['attributes']['rel'] . '" />';
    unset($el['#localized_options']['attributes']['rel']);
    $el['#localized_options']['html'] = TRUE;
    $link = l($img, $el['#href'], $el['#localized_options']);
    // clear leaf class for styling reasons.
    if (($key = array_search('leaf' , $el['#attributes']['class'])) !== FALSE) {
      unset($el['#attributes']['class'][$key]);
    }
    $attr = drupal_attributes($el['#attributes']);
    return sprintf("\n<li %s>%s</li>", $attr, $link);
  }
  return theme_menu_link($data);
}

/**
 * Theme wrapper function for the main menu links in the navigation area.
 */
function learnline4_menu_tree__menu_block__2($vars) {
  // outer tag replaced in menu-block-wrapper--2.tpl.php
  return '<ul class="userlistentrysub">' . $vars['tree'] . '</ul>';
}

/**
 * Theme wrapper function for the main menu links in the navigation area.
 */
function learnline4_menu_tree__menu_block__3($vars) {
  return '<ul>' . $vars['tree'] . '</ul>';
}

/**
 * Theme wrapper function for the main menu links in the footer area.
 */
function learnline4_menu_tree__menu_block__5($vars) {
  // outer tag replaced in menu-block-wrapper--5.tpl.php
  return $vars['tree'];
}

/**
 * Theme wrapper for responsive main menu.
 */
function learnline4_menu_tree__menu_block__6($vars) {
  var_dump($vars);
}

function learnline4_preprocess_menu_block_wrapper(&$variables) {
  switch ($variables['delta']) {
    // Manipulate menu_block__6 which is the responsive main menu
    case 6:
      $variables['theme_hook_original'] = 'learnline4_responsive_menu_block_wrapper';
//      var_dump($variables);die;
//      foreach ($variables['content'] as $first_level) {
//        var_dump($first_level['#title'], drupal_clean_css_identifier($first_level['#title']));
//        var_dump($first_level['#href']);
//      }
      break;
  }
}

/**
 * Theme wrapper function for the support navigation menu links.
 */
function learnline4_menu_tree__menu_block__menu_support_navigation($vars) {
  return '<ul>' . $vars['tree'] . '</ul>';
}

/**
 * Theme wrapper function for the organization menu links.
 */
function learnline4_menu_tree__menu_block__menu_organization_menu($vars) {
  return '<ul>' . $vars['tree'] . '</ul>';
}

/**
 * Implements template_preprocess_html().
 */
function learnline4_preprocess_html(&$variables) {
  $variables['meta_author'] = theme_get_setting('learnline4_html_meta_author');
  $variables['meta_copyright'] = theme_get_setting('learnline4_html_meta_copyright');
  $variables['meta_robots'] = theme_get_setting('learnline4_html_meta_robots');
  $variables['meta_description'] = theme_get_setting('learnline4_html_meta_description');
  $variables['meta_keywords'] = theme_get_setting('learnline4_html_meta_keywords');
  $logineo = SimpleSAML::getInstance()->getAttributes();
  if (!empty($logineo)) {
    $variables['loSchoolId'] = reset($logineo['loSchoolId']);
    $variables['loAppId'] = 'learnline';
  }
}

/**
 * Preprocess variables for page.tpl.php
 *
 * @see page.tpl.php
 */
function learnline4_preprocess_page(&$variables) {
  // Determine how many columns will be displayed:
  $variables['columns'] = 1;
  !empty($variables['page']['sidebar_first'])
      ? $variables['columns']++ : /* do nothing... */ null;
  !empty($variables['page']['sidebar_second'])
      ? $variables['columns']++ : /* do nothing... */ null;

  /**
   * Add Javascript:
   *
   * These scripts cannot be added via .info file, because of the individual
   * loading priority.
   */
  $path = drupal_get_path('theme', 'learnline4');
  $js_lib_opts = array(
    'scope' => 'header',
    'group' => JS_LIBRARY,
    'every_page' => TRUE,
    'type' => 'file',
  );

  $js_lib_opts['weight'] = 0;
  drupal_add_js($path . '/js/base64.js', $js_lib_opts);
  $js_lib_opts['weight'] = 100;
  drupal_add_js($path . '/js/learnline.js', $js_lib_opts);
  $js_lib_opts['weight'] = 110;
  drupal_add_js($path . '/js/learnline_page_overlay.js', $js_lib_opts);
  $js_lib_opts['weight'] = 120;
  drupal_add_js($path . '/js/learnline_toggleslider.js', $js_lib_opts);
  $js_lib_opts['weight'] = 100;
  $js_lib_opts['scope'] = 'footer';
  drupal_add_js($path . '/js/shariff.js', $js_lib_opts);
  $js_lib_opts['weight'] = 130;
  $js_lib_opts['scope'] = 'header';
  drupal_add_js($path . '/js/custom/articleLinker.js', $js_lib_opts);
  /*
	drupal_add_js('new articleLinker().init();',
    array('type' => 'inline', 'scope' => 'footer', 'weight' => 5)
  );
	*/

  $menu_item = menu_get_item();
  if ($menu_item['path'] === 'blog') {
    drupal_set_title(_('Themenfelder'));
  }
}

/**
 * Perform alterations to the content of a block.
 *
 * This hook allows you to modify any data returned by hook_block_view().
 *
 * Note that instead of hook_block_view_alter(), which is called for all
 * blocks, you can also use hook_block_view_MODULE_DELTA_alter() to alter a
 * specific block.
 *
 * @param $data
 *   The data as returned from the hook_block_view() implementation of the
 *   module that defined the block. This could be an empty array or NULL value
 *   (if the block is empty) or an array containing:
 *   - subject: The default localized title of the block.
 *   - content: Either a string or a renderable array representing the content
 *     of the block. You should check that the content is an array before trying
 *     to modify parts of the renderable structure.
 * @param $block
 *   The block object, as loaded from the database, having the main properties:
 *   - module: The name of the module that defined the block.
 *   - delta: The unique identifier for the block within that module, as defined
 *     in hook_block_info().
 *
 * @see hook_block_view_MODULE_DELTA_alter()
 * @see hook_block_view()
 */
function learnline4_block_view_wp_blog_date_navigation_alter(&$data, $block) {
  unset($data['content']['show_all_link']);
}

/**
 * Theme handler for theme('wp_blog_archive).
 */
function learnline4_wp_blog_archive($element) {
  $items = array();
  $archive = $element['element']['archive'];
  foreach ($archive as $year) {
    $item = array(
      'data' => t('!year (@count)', array('!year' => l($year['text'], $year['url']), '@count' => $year['count'])),
      'children' => array(),
    );
    $items[] = $item;
    foreach ($year['months'] as $month) {
      $item = array(
        'data' => t('!month (@count)', array('!month' => l($month['text'], $month['url']), '@count' => $month['count'])),
      );
      $items[] = $item;
    }
  }
  return theme('item_list', array(
    'items' => $items,
  ));
}

/**
 * Implements hook_views_post_render().
 */
function learnline4_views_post_render(&$view, &$output, &$cache) {
  // Add an appropriate menu-trail for the blog_tags view.
  if ($view->name === 'blog_tags' && strpos($_GET['q'], 'taxonomy/term/') === 0) {
    // wp_blog default trail is: Home >> Blog
    $trail = array(
      array(
        'title' => t('Home'),
        'href' => '<front>',
        'localized_options' => array(),
        'type' => MENU_NORMAL_ITEM,
      ),
      array(
        'title' => t('Blog'),
        'href' => 'blog',
        'localized_options' => array(),
        'type' => MENU_NORMAL_ITEM,
      ),
    );
    // Views tries to set the breadcrumb. Reset it so it's controlled by the
    // active menu trail instead.
    drupal_static_reset('drupal_set_breadcrumb');
    menu_set_active_trail($trail);
  }
}

/**
 * Implements hook_css_alter().
 */
function learnline4_css_alter(&$css) {
  if(url($_GET['q'], array()) === '/content/redaktion') {
    unset($css[drupal_get_path('module','system').'/system.theme.css']);
  }
  unset($css[drupal_get_path('module','system').'/system.menus.css']);
  // For each item, don't allow preprocessing to disable @import.
  foreach ($css as &$item) {
    if (file_exists($item['data'])) {
      $item['preprocess'] = FALSE;
    }
  }
}

/**
 * Implements theme_js_alter().
 */
function learnline4_js_alter(&$js) {
  $theme_path = drupal_get_path('theme', 'learnline4');
  if (arg(0) === 'suche') {
    $js[$theme_path . '/js/search-results.js'] = array(
      'group' => 0,
      'type' => 'file',
      'scope' => 'header',
      'data' => $theme_path . '/js/search-results.js',
      'weight' => 100,
      'defer' => FALSE,
      'cache' => TRUE,
      'every_page' => TRUE,
      'preprocess' => TRUE,
      'requires_jquery' => TRUE,
    );
  }
}

/**
 * Implements theme_link().
 *
 * @param $variables
 *   An associative array containing:
 *   - links: An associative array of links to be themed. The key for each link
 *     is used as its CSS class. Each link should be itself an array, with the
 *     following elements:
 *     - title: The link text.
 *     - href: The link URL. If omitted, the 'title' is shown as a plain text
 *       item in the links list.
 *     - html: (optional) Whether or not 'title' is HTML. If set, the title
 *       will not be passed through check_plain().
 *     - attributes: (optional) Attributes for the anchor, or for the <span>
 *       tag used in its place if no 'href' is supplied. If element 'class' is
 *       included, it must be an array of one or more class names.
 *     If the 'href' element is supplied, the entire link array is passed to
 *     l() as its $options parameter.
 *   - attributes: A keyed array of attributes for the UL containing the
 *     list of links.
 *   - heading: (optional) A heading to precede the links. May be an
 *     associative array or a string. If it's an array, it can have the
 *     following elements:
 *     - text: The heading text.
 *     - level: The heading level (e.g. 'h2', 'h3').
 *     - class: (optional) An array of the CSS classes for the heading.
 *     When using a string it will be used as the text of the heading and the
 *     level will default to 'h2'. Headings should be used on navigation menus
 *     and any list of links that consistently appears on multiple pages. To
 *     make the heading invisible use the 'element-invisible' CSS class. Do not
 *     use 'display:none', which removes it from screen-readers and assistive
 *     technology. Headings allow screen-reader and keyboard only users to
 *     navigate to or skip the links. See
 *     http://juicystudio.com/article/screen-readers-display-none.php and
 *     http://www.w3.org/TR/WCAG-TECHS/H42.html for more information.
 */
function learnline4_links($variables) {
  $links = $variables['links'];
  $key = 'node-readmore';
  if (count($links) === 1 && isset($links[$key])) {
    $link = $links[$key];
    if (isset($link['href'])) {
      // Pass in $link as $options, they share the same keys.
      return l($link['title'] . '&nbsp;&raquo;', $link['href'], $link);
    }
  }
  return theme_links($variables);
}

/**
 * Returns HTML for a query pager.
 *
 * Menu callbacks that display paged query results should call theme('pager') to
 * retrieve a pager control so that users can view other results. Format a list
 * of nearby pages with additional query results.
 *
 * @param $variables
 *   An associative array containing:
 *   - tags: An array of labels for the controls in the pager.
 *   - element: An optional integer to distinguish between multiple pagers on
 *     one page.
 *   - parameters: An associative array of query string parameters to append to
 *     the pager links.
 *   - quantity: The number of pages in the list.
 *
 * @ingroup themeable
 */
function learnline4_pager($variables) {
  $variables['tags'][0] = '« ';
  $variables['tags'][1] = '‹ ';
  $variables['tags'][3] = ' ›';
  $variables['tags'][4] = ' »';
  return theme_pager($variables);
}

/**
 * Implements hook_page_alter().
 */
function learnline4_page_alter(&$page) {
  /**
   * Alter layout for front page:
   */
  if (drupal_is_front_page()) {
    $count = 0;
    // Change elements of content region on the front page.
    $region_name = 'content';
    foreach (element_children($page[$region_name]) as $block_name) {
      $block = &$page[$region_name][$block_name];
      // Clear block theme wrapper.
      $block['#theme_wrappers'] = array();
      // Add #prefix and #suffix to a block to wrap a div around it.
      switch(++$count) {
        // open 1st line
        case 1:
          $block['#prefix'] = '<div class="line" id="line1">' . '<div class="kasten _50">';
          $block['#suffix'] = '</div>';
          break;
        case 2:
          $block['#prefix'] = '<div class="kasten _50">';
          $block['#suffix'] = '</div>';
          break;
        case 3:
          $block['#prefix'] = '<div class="kasten _50">';
          $block['#suffix'] = '</div></div>';
          break;
        // open 2nd line
        case 4:
          $block['#prefix'] = '<div class="line" id="line2"><div class="kasten _100">';
          $block['#suffix'] = '</div>';
          break;
        case 5:
          $block['#prefix'] = '<div id="herausgeber" class="kasten _50">';
          $block['#suffix'] = '</div>' . '</div>'; // close 2nd line
          break;
        // open 3rd line
        case 6:
          $block['#prefix'] = '<div class="line" id="line3"><div id="twitterLeft" class="kasten _50">';
          $block['#suffix'] = '</div>';
          break;
        case 7:
          /*
<a href="https://twitter.com/intent/user?screen_name=learnline_nrw" class="twitter-follow-button" data-show-count="false" data-lang="de">@learnline_nrw folgen</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?\'http\':\'https\';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+\'://platform.twitter.com/widgets.js\';fjs.parentNode.insertBefore(js,fjs);}}(document, \'script\', \'twitter-wjs\');</script>
           */
          $block['#prefix'] = '<div id="twitter" class="kasten _50 fit-height">' .
                '<h2><span class="headline">Folge uns auf Twitter</span><br />Tweets</h2>' .
                  '<div id="twitter-follow"><a href="https://twitter.com/intent/user?screen_name=learnline_nrw" target="_blank">' .
                  '<div>&emsp;&ensp;@learnline_nrw folgen</div></a></div>' .
                '<div id="twitter-inner">';
          $block['#suffix'] = '</div>' .
                '<p style="clear:both;float:left"><a class="boxlinkbutton" href="https://twitter.com/learnline_nrw" target="_blank">' .
                'mehr ...</a></p></div>'; // close 1st line
          break;
        case 8:
          $block['#prefix'] = '<div id="twitterRight" class="kasten _50">';
          $block['#suffix'] = '</div>' . '</div>'; // close 3rd line
          return;
      }
    }
  }
}

/**
 * Implements theme_field__field_image().
 *
 * Uses fields 'field_image' and 'field_image_caption' to create image captions
 * where applicable.
 */
function learnline4_field__field_image($variables) {
  $output = '';
  $figcaption = '';
  // Consider to handle multiple images:
  foreach ($variables['items'] as $delta => $item) {
    // Render the image itself:
    $image = drupal_render($item);

    /**
     * Because of unification we have to add the 'kastenimg' CSS class...
     */
    // Let's take a look for an existing 'class' attribute.
    if ($pos = strpos($image, ' class=')) {
      $image = substr_replace($image, 'kastenimg ', $pos + 8, 0);
    }
    else {
      $pos = strpos($image, '<img');
      $image = substr_replace($image, ' class="kastenimg"', $pos + 4, 0);
    }

    // Check if corresponding caption is available:
    if (isset($variables['element']['#object']->field_image_caption)) {
      $caption = reset($variables['element']['#object']->field_image_caption);
    }
    // Create caption if applicable:
    if (isset($caption) && isset($caption[$delta]) && !empty($caption[$delta]['value'])) {
      $figcaption .= '<figcaption>' . $caption[$delta]['value'] . '</figcaption>';
    }
    // Differenciate image styles:
    switch ($item['#image_style']) {
      case 'thumbnail':
        $before = '<figure class="custom-field-image">';
        $after = $figcaption . '</figure>';
        $output .= preg_replace('/(<.+>)(<.+>)(<.+>)/',
            "$1$before$2$after$3", $image);
        break;
      default:
        $output .= '<figure class="custom-field-image">' . $image .
            $figcaption . '</figure>';
        break;
    }
  }

  return $output;
}

/**
 * Transform an input text to a HTML id/class attribute save value.
 *
 * @param $text Input text.
 * @return mixed Escaped/Transformed input text.
 */
function _learnline4_html_class($text) {
  return preg_replace(
    array('/ä/', '/ö/', '/ü/', '/Ä/', '/Ö/', '/Ü/', '/ß/', '/€/', '/[^a-zA-Z0-9]|[\-]+/'),
    array('ae', 'oe', 'ue', 'Ae', 'Oe', 'Ue', 'ss', 'Euro', '-'),
    $text
  );
}
