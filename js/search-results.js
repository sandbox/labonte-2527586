(function($) {
  var articleLinkerCalls = 0,
      articleLinkerCallsRequired = 2;

  learnline.loadArticleLinker = function() {
    articleLinkerCalls++;

    if (articleLinkerCalls === articleLinkerCallsRequired) {
      new articleLinker().init();
      var shariff = $('.shariff');

      if (shariff.length > 0) {
        shariff.each(function(key, value) {
          new Shariff($(value));
        });
      }
    }
  }
})(jQuery);

