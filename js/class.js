// $Id:$

/**
 * @file
 * Declaring an extendable base class for object-oriented coding.
 */

var Class = function (className, definition) {
  var klass = function () {
    this.initialize.apply(this, arguments);
  };

  for (var method in definition) {
    klass.prototype[method] = definition[method];
  }

  if (!klass.prototype.initialize) {
    klass.prototype.initialize = function () {};
  }

  return klass;
};
