(function ($) {
    /**
     * Represents the filter menu with customized settings and event bindings
     * @returns {filterMenu}
     * @requires Uses the mmenu jquery plugin
     */
    filterMenu = function () {

        this._menu = $('#filter-menu');
        this._title = $(this._menu).find('.filter-title');
        this._content = $(this._menu).find('.filter-content');
        this._body = $(this._menu).find('.filter-body');
        this._filterArrow = $(this._menu).find('.filter-arrow');

        this._footer = $(this._menu).find('.filter-footer');
    };

    /**
     * Initializes the mobile menu
     * @returns {void}
     */
    filterMenu.prototype.init = function () {
        this._isClosed = true;
        this._isMoving = false;
        this._calcBodyHeight();

        var me = this;
        $(this._title).click(function () {
            me._toggleContent();
        });
        $(this._menu).find('.filter-close').click(function () {
            me._closeContent();
        });
        $(window).resize(function () {
            me._calcBodyHeight();
        });
        this._initAccordions();
        this._initCalendars();
        //this._initSelectLists();
    };

    filterMenu.prototype.hide = function () {
        $(this._title).slideUp(300);
    };

    filterMenu.prototype.show = function () {
        $(this._title).slideDown(300);
    };

    /**
     * Initializes the accordions within the filter menu
     */
    filterMenu.prototype._initAccordions = function () {
        $('.filter-accordion').accordion({collapsible: true, heightStyle: "content"});
    };

    /**
     * Initializes the calendars within the filter menu
     */
    filterMenu.prototype._initCalendars = function () {
        // $('.filter-calendar').each(function (index, calendar) {
        //     var fieldId = $(calendar).data('field');
        //     $(calendar).datepicker({altField: '#' + fieldId, width: 200});
        // });
    };


    /**
     * Initializes the select lists within the filter menu
     */
    filterMenu.prototype._initSelectLists = function () {
        $('.filter-selectList').each(function (index, selList) {
            var list = new selectList(selList);
            list.init();
        });
    };

    /**
     * calculates the maximum body height so the fitler menu title doesn't exceed
     * the top of the viewport
     * @returns {void}
     */
    filterMenu.prototype._calcBodyHeight = function () {
        var winHeight = $(window).height();
        var titleHeight = $(this._title).outerHeight();
        var footerHeight = $(this._footer).outerHeight();
        var bodyHeight = Math.max(0, winHeight - titleHeight - footerHeight);
        $(this._body).css({maxHeight: bodyHeight});
    };

    /**
     * Binds events to the mmenu (not yet implemented)
     * @returns {void}
     */
    filterMenu.prototype._toggleContent = function () {
        if (this._isMoving) {
            return;
        }
        if (this._isClosed) {
            this._openContent();
        }
        else {
            this._closeContent();
        }
    };

    /**
     * Function executed when the menu is opened
     * @returns {void}
     */
    filterMenu.prototype._openContent = function () {
        this._isMoving = true;
        globals.blockScroll();
        var zIndex = parseInt($(this._menu).css("zIndex"));
        globals.showOverlay(zIndex - 1);
        this._calcBodyHeight();
        $(this._filterArrow).removeClass('fa-caret-up');
        $(this._filterArrow).addClass('fa-caret-down');
        var me = this;
        $(this._content).slideDown(300, function () {
            me._isMoving = false;
            me._isClosed = false;
        });
    };


    /**
     * Function executed when the menu is closed
     * @returns {void}
     */
    filterMenu.prototype._closeContent = function () {
        this._isMoving = true;
        globals.unblockScroll();

        $(this._filterArrow).removeClass('fa-caret-down');
        $(this._filterArrow).addClass('fa-caret-up');
        var me = this;
        $(this._content).slideUp(300, function () {
            me._isMoving = false;
            me._isClosed = true;
            globals.hideOverlay();
        });
    };
})(jQuery);
