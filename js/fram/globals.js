var globals = {
    blockScroll: function() {
        jQuery('body').css({overflow: 'hidden', height: '100%'});
    },
    unblockScroll: function() {
        jQuery('body').css({overflow: 'auto', height: 'auto'});
    },
    
    showOverlay : function(zIndex)
    {
        var overlay = jQuery('<div id="overlay"></div>');
        jQuery(overlay).css(
                {"zIndex": zIndex, backgroundColor: "black", opacity: 0.5, position: "fixed",
                width: "100%", height: "100%", display: "none"});
        jQuery('body').prepend(overlay);
        jQuery(overlay).fadeIn();
    },
    
    hideOverlay : function()
    {
        var overlay = jQuery("#overlay");
        jQuery(overlay).fadeOut(400, function(){jQuery(overlay).remove();});
    }
};

