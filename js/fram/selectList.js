/**
 * A pretty select list 
 * @param {string|jQuery} element The ul element containing the select list,
 * must define data-field with id of an related hidden field
 * @returns {selectList}
 */
selectList = function(element){
    this._list = jQuery(element);
    this._items = jQuery(element).find('li');
    this._field = jQuery('#' + jQuery(element).data('field'));
};

/**
 * Initializes the select list
 */
selectList.prototype.init = function() {
    var me = this;
    jQuery(this._items).each(function(idx, item) {
        me._initItem(item);
    });
};


/**
 * Initializes a single item in the select list by adding necessary click handler
 * @param {jQuery} item The item
 */
selectList.prototype._initItem = function(item) {
    if (jQuery(item).hasClass('active')) {
        jQuery(this._field).val(this._getValue(item));
    }
    var me = this;
    jQuery(item).click(function(){me._itemSelect(item);});
};

/**
 * Gets the current value of an item
 * @param {jQuery} item
 * @returns {string}
 */
selectList.prototype._getValue = function(item) {
    return jQuery(item).data('value');
};

/**
 * Selects the item by adding the 'active' class and setting value on related hidden field
 * @param {jQuery} item The item
 */
selectList.prototype._itemSelect = function(item) {
    jQuery(this._items).removeClass('active');
    jQuery(item).addClass('active');
    jQuery(this._field).val(this._getValue(item));
};




