(function ($) {

    var getWidth = function() {
        var myWidth = 0;
        var myHeight = 0;
        if (typeof( window.innerWidth ) == 'number') {//Non-IE
            myWidth = window.innerWidth;
            myHeight = window.innerHeight;
        } else if (document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight )) {//IE 6+ in 'standards compliant mode'
            myWidth = document.documentElement.clientWidth;
            myHeight = document.documentElement.clientHeight;
        } else if (document.body && ( document.body.clientWidth || document.body.clientHeight )) {//IE 4 compatible
            myWidth = document.body.clientWidth;
            myHeight = document.body.clientHeight;
        }
        return [myWidth];
    };

    $.fn.fitHeights = function () {
        var items = $(this);
        function setHeights() {
            var currentTallest = 0;
            items.css({'min-height': currentTallest});  // unset min-height to get actual new height
            // right now this causes a noticeable shift in height on resize. workarounds?
            items.each(function () {
                if ($(this).height() > currentTallest) {
                    currentTallest = $(this).height();
                }
            });
            if (getWidth() > 768) {
                items.css({'min-height': currentTallest});
            }
        }
        setHeights();
        $(window).on('resize', setHeights);
        return this;
    };

    $(document).ready(function () {
        if (getWidth() > 768) {
            $('#line1 .kasten').fitHeights();
            $('#line2 .kasten').fitHeights();
            $('#line3 .kasten').fitHeights();
            $('#line4 .kasten').fitHeights();
            $('#line1 .kasten1').fitHeights();
            $('#line2 .kasten1').fitHeights();
            $('#line3 .kasten1').fitHeights();
            $('#line4 .kasten1').fitHeights();
            $('#content .height').fitHeights();
        }
    });

})(jQuery);
