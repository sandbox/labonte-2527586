// $Id:$

/**
 * @file
 * Module's Javascript snippets.
 *
 * @todo Consider renaming function uebersicht().
 */

(function (window, $) {

learnline.Class('ToggleSlider', {
  initialize: function (trigger, target, options) {
    this.trigger = $(trigger);
    this.target = $(target);
    this.options = options || {
      callback: [ this.stdCallback ],
      observer: [ this.stdObserver ],
      cursor: 'pointer',
      state: this.target.is(':visible')
    };
    this.visible = this.target.is(':visible');

    if (this.options.callback === undefined || this.options.callback.length < 1)
      this.options.callback = [ this.stdCallback ];

    if (this.options.observer === undefined || this.options.observer.length < 1)
      this.options.observer = [ this.stdObserver ];

    this.exec();
  }, //function

  exec: function () {
    var self = this;
    $(this.options.observer).each(function () {
      this(self);
    });
  },

  stdObserver: function (self) {
    self.trigger.css('cursor', self.options.cursor).click(function () {
      $(self.options.callback).each(function () {
        this(self);
      });
    });

    // Correct state/visibility, if the given options differ from the style
    // actually applied to our element.
    if (self.options.state !== self.visible) {
      $(self.options.callback).each(function () {
        this(self);
      });
    }
  },

  stdCallback: function (self) {
    self.trigger[self.visible ? 'removeClass' : 'addClass']('current');
    self.target[self.visible ? 'removeClass' : 'addClass']('current');
    self.target[self.visible ? 'slideUp' : 'slideDown']('fast');
    self.visible = !self.visible;
  }
}); //Class

})(window, jQuery);
