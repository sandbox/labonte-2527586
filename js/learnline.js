// $Id:$

/**
 * @file
 * Theme's Javascript classes and snippets.
 */

(function ($) {

  window.setTimeout(function() {
    if ($(window).width() >= 768) {
      var twitterLeft = $('#twitterLeft'),
        twitter = $('#twitter'),
        twitterRight = $('#twitterRight'),
        height = $('#line3').height();

      twitterLeft.css('height', height);
      twitter.css('height', height);
      twitterRight.css('height', height);
    }
  }, 3000);

  // Workaround for browsers that don't implement the console object.
  if (!window.console) {
    window.console = {
      log: function (variable) { alert(variable); }
    };
  }

  // Working around the missing base64 support in MSIE <10
  if (!window.atob) window.atob = base64.decode;
  if (!window.btoa) window.btoa = base64.encode;

  // Workaround for missing String.trim()-method in MSIE
  if (!String.trim) String.prototype.trim = function () {
    return this.replace(/^\s+|\s+$/g, '');
  }

  window.learnline = {

    /**
     * Turn on/off debugging features.
     * Use as bitwise operator.
     *
     * Possible values:
     *  0: Off
     *  1: Stores class instances in objects
     *  2: Logs to the console
     */
    //  DEBUG: 3,
    DEBUG: 0,

    /**
     * If debugging bit '1' is enabled, this object stores all object instances
     * of Javascript learnline classes.
     */
    objects: {},

    /**
     * Base class definition.
     * Used to simplify definition of further learnline classes.
     *
     * @todo Implement inheritance.
     *
     * @param   {string} name
     * @param   {Object} definition
     * @returns {learnline.Class}
     */
    Class: function (name, definition) {
      // Create our class object.
      var klass = function () {
        this.initialize.apply(this, arguments);
        this.postInitialization.apply(this);
      };

      // Assign class name/type and DEBUG value.
      klass.prototype.type = name;
      klass.prototype.DEBUG = this.DEBUG;

      // Assign class methods.
      for (var method in definition) {
        klass.prototype[method] = definition[method];
      }

      // Check for a constructor and assign one if none is given.
      if (!klass.prototype.initialize) {
        klass.prototype.initialize = function () {};
      }

      // Assign postInitialization method if it has not been overwritten.
      if (!klass.prototype.postInitialization) {
        klass.prototype.postInitialization = function () {
          this.DEBUG&1 && window.learnline.objects[this.type].push(this);
          this.DEBUG&2 && console.log('Initialized new object of class "' + this.type + '".');
        };
      }

      // Initialize an array as storage for all objects of the new class' type.
      this.DEBUG&1 && (window.learnline.objects[name] = []);
      // Add class to the global learnline object.
      window.learnline[name] = klass;

      // Finally return the resulting class
      this.DEBUG&2 && console.log('Created new class: "' + klass.prototype.type + '".');
      return klass;
    },

    /**
     * Compare two version strings.
     *  a > b returns >0
     *  a < b returns <0
     *  a = b returns 0
     *
     * @param   {String} a A version string.
     * @param   {String} b The version string to compare with.
     * @returns {Integer}
     */
    compareVersions: function (a, b) {
      var ver = /(\.0)+[^\.]*$/;

      a = (a + '').replace(ver, '').split('.');
      b = (b + '').replace(ver, '').split('.');

      var len = Math.min(a.length, b.length);
      for (var i = 0; i < len; i++) {
        var cmp = parseInt(a[i], 10) - parseInt(b[i], 10);
        if (cmp !== 0) {
          return cmp;
        }
      }

      return a.length - b.length;
    },

    /**
     * Get cookies as object.
     *
     * @returns {{ object }}
     */
    getCookies: function() {
      var cookiesList = document.cookie.split(';'),
        cookies = {};

      $(cookiesList).each(function() {
        var parts = this.split('=');
        cookies[ parts[0].trim() ] = decodeURIComponent( parts[1].trim() );
      });

      return cookies;
    },

    /**
     * Get a specific cookie value.
     *
     * @param {{ String }} name
     * @returns {{ String }}
     */
    getCookie: function(name) {
      var cookies = this.getCookies();

      if (typeof cookies[name] === 'undefined') {
        return false;
      }

      return cookies[name];
    },

    /**
     * Set a cookie.
     *
     * @param {{ String }} name
     * @param {{ String }} value
     */
    setCookie: function(name, value, expiration) {
      var date = new Date();

      if (typeof expiration === 'undefined') {
        // By default the cookie will expire after 90 days.
        expiration = 90 * 24 * 3600;
      }
      date.setTime(date.getTime() + expiration);

      document.cookie = name + '=' + encodeURIComponent(value) + '; expires=' + date.toUTCString() + '; path=/';
    },

    assureLocationSelection: function(originatingLink) {
      var currentLocation = this.getCookie('lls_school_location');
      if ((!currentLocation || currentLocation.length < 1) && $(originatingLink).data('provider').toUpperCase() === 'EDMOND') {
        var href = $(originatingLink).attr('href');
        $('#modal-choose-location').data('originatingLink', href).modal('show');
        return true;
      }
      return false;
    },

    submitLocation: function(isSearchResultPage) {
      if (isSearchResultPage) {
        var originatingLink = $('#modal-choose-location').data('originatingLink');
        if (typeof originatingLink !== 'undefined' && originatingLink.length > 0) {
          window.open(originatingLink);
          $('#modal-choose-location').data('originatingLink', '');
        }
        return true;
      } else {
        $('#search-button').click();
        return false;
      }
    }
  };

  // Check for jQuery version.
  if (learnline.DEBUG&2 && learnline.compareVersions($.fn.jquery, '1.7') < 0) {
    console.log('Out-dated jQuery version in use! Please update...');
  }

  // Poll page until
  var llsCounter = 0;
  var llsInterval = setInterval(function() {
    if ($('.llsLocationValue').length > 0) {
      llsCounter += setLLSLocation('.llsLocationValue');
      if (llsCounter >= 2) {
        clearInterval(llsInterval);
      }
    }
  }, 1000);

  /**
   * Sets the learnline school location cookie to restore the user selection on next page visit.
   *
   * @param value The key of the selected school location.
   */
  window.setLLSLocationCookie = function(value) {
    var expireDate = new Date();
    expireDate.setFullYear(expireDate.getFullYear() + 1);
    document.cookie = 'lls_school_location=' + value + '; expires=' + expireDate.toGMTString() + '; path=/';
  }

  window.setLLSLocation = function(selector) {
    var selectorElementCount = 0;
    var location = learnline.getCookie('lls_school_location');
    if (location != '' && typeof llsLocations !== 'undefined' && typeof llsLocations[location] !== 'undefined') {
      $(selector).each(function () {
        $(this).html(llsLocations[location]);
        if (!$(this).data('processed')) {
          $(this).data('processed', true);
          selectorElementCount++;
        }
      });
      var sidebar_filter = $('.llsLocationFacetValue');
      if (sidebar_filter.length > 0) {
        sidebar_filter.each(function() {
          $(this).html(llsLocations[location]);
        });
      }
    }

    return selectorElementCount;
  }

  var edmondHref = '';

  window.submitSearchForm = function(isSearch) {

    // Use the href attribute of the link element
    // if no EDMOND dialog is available and when
    // the search page are active
    if (isSearch && (typeof edmondHref === 'undefined' || edmondHref === '')) {
      return true;
    }

    // Special behaviour if the EDMOND href var is set
    // Close modal dialog, set cookie and open the result link
    if (typeof edmondHref !== 'undefined' && edmondHref !== '') {
      jQuery('#locationModalBtnClose').click();
      setLLSLocation('llsLocationValue');

      var location = getCookie('lls_school_location');
      if (location === '') {
        location = '111';
      }

      edmondHref = edmondHref.replace('//', '');
      edmondHref = edmondHref.replace(/\/([a-zA-Z_0-9-]+)\/$/gi, '');
      edmondHref += '/' + location + '/'

      window.open(edmondHref, '_blank');
      edmondHref = '';
      return true;
    }

    // The default homepage behaviour.
    // Click the search button to start a search on location select
    else {
      jQuery('#search-button').click();
    }

    return false;
  }

})(jQuery);
