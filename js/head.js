// $Id:$

/**
 * @file
 * Provides theme and layout relevant Javascript calls.
 */

(function ($) {

/**
 * Implements accordion functionality on 'content/service'.
 */
function service_accordion(answer, question) {
  // Return immediately if there are no questions/answers.
  if (!answer.length || !question.length) {
    return;
  }
  answer.hide();
  question.click(function () {
    question.removeClass('current');
    answer.not(':hidden').slideUp('fast');
    var current = $(this);
    current.next().not(':visible').slideDown('fast', function () {
      current.addClass('current');
    });
  });
}


function myusermenu(answer,question) {
  if (!answer.length || !question.length) return;
  answer.hide();
  question.click( function(e) {
    e.preventDefault();
    question.removeClass("current");
    answer.not(":hidden").slideUp('fast');
    var current = $(this);
    $(this).next().not(":visible").slideDown('fast',function() {
      current.addClass("current");
    });
  });
}


function mynavi(answer,question) {
  if (!answer.length || !question.length) {
    return;
  }
//    answer.hide();
  question.click( function() {
    question.removeClass("current");
    answer.not(":hidden").slideUp('fast');
    var current = $(this);
    $(this).next().not(":visible").slideDown('fast',function() {
      current.addClass("current");
    });
  });
}

function uebersicht(answer,question) {
  if (!answer.length || !question.length) {
    return;
  }

  answer.hide();
  question.click(function () {
    var current = $(this);
    question.removeClass('current');
    answer.not(':hidden').slideUp('fast');

    $(this).next().not(':visible').slideDown('fast', function () {
        current.addClass('current');
    });
    // Anpassung publicplan 13.11.2013
    $('#advanced-search-container').not(':visible').slideDown('fast', function () {
      current.addClass('current');
    });
  });
} //function


$(document).ready(function() {
  // service_accordion()
  service_accordion($('.answer'), $('.question'));
  $('.current').next().not(':visible').slideDown('fast');

  // myusermenu()
  myusermenu( $(".userlistentrysub"), $(".titel") );
  $(".current").next().not(":visible").slideDown('fast',function() { });

  // mynavi():
  mynavi( $("#left"), $(".navi") );
  $(".current").next().not(":visible").slideDown('fast',function() { });

  // For backward compatibility (considering the Drupal contains the old
  // fashioned tooltipster tooltips somewhere in the content), we have to
  // remove the 'tooltip' class (which is used from bootstrap for the
  // actual tooltip element) and add the bootstrap tooltip.
  $('.tooltip').removeClass('tooltip').data('placement', 'top').tooltip();
  $('[data-toggle="tooltip"]').tooltip();

  var answer1 = $('.answer1');
  $('.question1').each(function (idx) {
    new learnline.ToggleSlider(this, answer1[idx], { state: false });
  });
  
  $('a.onpage').each(function () {
    new learnline.PageOverlay(this);
  });

//  $('#Form1').submit(function () {
//    var q = $('#p_searchword');
//    this.action += q.val();
//    q.attr('disabled', true);
//  });

//  uebersicht($(".answer1"), $(".question1"));
//  $('.current').next().not(':visible').slideDown('fast', function() { });
//  $('.current').next().not(':visible').slideDown('fast');

//  $('#Themen').click (function() {
//    $(".answer1").slideDown('fast');
//    $(".question1").addClass("current");
//  });



  /**
   * Initialize Alertify
   */

  /* Define alertify message types. */
  var mTypes = {
    "status": ["success", 0],
    "warning": ["log", 0],
    "error": ["error", 0]
  };
  /* Transform Drupal messages to alertify log outputs. */
  for (c in mTypes) {
    /* Find messages of each type. */
    var m = $(".messages." + c);
    /* Call alertify for each message and the appropriate type. */
    if (m.find("li").length > 1) {
      $(m.find("li")).each(function () {
        $$.log($(this).html(), mTypes[c][0], mTypes[c][1]);
      });
    } else if (m.length === 1) {
      $$.log(m.html(), mTypes[c][0], mTypes[c][1]);
    }
    /* Detach the original message produced by drupal. */
    m.detach();
  }
});

  //  $(document).ready(function() {SameWidth('.footerlistentrysub');});
})(jQuery);
