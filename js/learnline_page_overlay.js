// $Id:$

/**
 * @file
 * Page overlay class.
 */

(function (window, $) {
  /**
   * class PageOverlay
   */
  var PageOverlay = learnline.Class('PageOverlay', {
    /**
     * Constructor.
     *
     * @param   {string} trigger Element selector/reference to observe.
     * @param   {string} content Element selector/reference for on-page content or request URL for AJAX content to show.
     * @param   {object} options Sorry review the code to see what options are available...
     * @returns {PageOverlay} New instance of this class.
     */
    initialize: function (trigger, content, options) {
      this.docbody = $('body');
      this.overlay = $('#page-overlay');
      this.wrapper = $('#page-overlay-wrapper');
      this.head = $('#page-overlay-head');
      this.body = $('#page-overlay-body');
      this.close = $('#page-overlay-close');
      this.loadingIndicator = this.overlay.css('backgroundImage');
      this.trigger = $(trigger);
      this.options = options || {
        observer: [ this.stdObserver ],
        callback: [ this.stdCallback ]
      };

      // Check whether the content is a request URL:
//      var url_type = /^.+:\/\/.+\..+$/;
      var url_type = /(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w\.-=?]*)*\/?/;
      if (this.trigger.attr('href') !== undefined && this.trigger.attr('href').match(url_type)) {
        this.url = this.trigger.attr('href');
        this.trigger.attr('href', '#onpage_link');
        this.trigger.attr('onclick', 'return false;');
      } //if
      else {
        this.content = $(content);
      } //else

      // Add default observer, if none is set yet:
      if (this.options.observer === undefined || this.options.observer.length < 1) {
        this.options.observer = [ this.stdObserver ];
      } //if

      // Add default callback, if none is set yet:
      if (this.options.callback === undefined || this.options.callback.length < 1) {
        this.options.callback = [ this.stdCallback ];
      } //if

      // Apply all observers:
      this.trigger.click(function () { return false; });
      var self = this;
      $(self.options.observer).each(function () {
        this(self);
      });
    }, //function

    /**
     * This should be called by observers to execute the callbacks.
     */
    exec: function () {
      var self = this;
      $(self.options.callback).each(function () {
        this(self);
      });
    }, //function

    /**
     * The default observer for the given trigger.
     */
    stdObserver: function (self) {
      self.trigger.click(function () {
        self.exec();
      });
      self.body.hover(
          function () {
            self.close.removeClass('highlight');
          },
          function () {
            self.close.addClass('highlight');
          }
      );
    }, //function

    /**
     * The default page overlay callback to use.
     *
     * @param {PageOverlay} self The current page overlay class instance.
     */
    stdCallback: function (self) {
      // Get scroll position (Y axis):
      var posY = self.getScrollPosition();
      // Fix body and set inverted posY as CSS attribute top:
      self.docbody.addClass('overlayed').css('top', '-' + posY + 'px');
      // Avoid propagation of click events on the overlay body:
      self.body.click(function (e) { e.stopPropagation(); });
      // Show overlay and apply click event for closing:
      self.overlay.fadeIn('fast')
          .click(function () {
            self.docbody.removeClass('overlayed').css('top', 'auto');
            self.overlay.fadeOut('fast');
            self.wrapper.delay(250).hide();
            window.scroll(0, posY);
            setTimeout(function () {
              self.overlay.css('backgroundImage', self.loadingIndicator);
            }, 500);
          });

      /**
       * Fill overlay wrapper with content:
       */

      // Check if the target file extension indicates an image.
      var img_url = /^.+\.(jpg|jpeg|gif|png|bmp)$/i;
      if (self.url && self.url.match(img_url)) {
        var width = 0;
        var height = 0;
        var img = $('<img/>').attr('src', self.url).css({
              display: 'block',
              margin: '0 auto',
              'max-width': '100%',
              width: 'auto',
              height: 'auto',
              padding: '2px',
              border: '1px solid #ddd'
            });
        self.body.html(img);
        
        setTimeout(function () {
          self.overlay.css('backgroundImage', 'none');
        }, 250);
        self.wrapper.delay(250).slideDown('fast');
      }
      // Check whether the target is an URL.
      else if (self.url) {
        var glue = !self.url.search(/\?/) ? '?' : '&';
        // Stage ajax request to retrieve content:
        $.ajax(self.url + glue + 'ajax=1')
            .complete(function () {
              setTimeout(function () {
                self.overlay.css('backgroundImage', 'none');
              }, 200);
            })
            .success(function (response) {
              // Handle regular/text HTTP response:
              self.body.html(response);
              self.wrapper.delay(250).slideDown('fast');
            })
            .error(function (r) {
              self.body.empty();
              $(r.responseText).find('.messages').each(function () {
                self.body.append(this);
              });
              self.wrapper.delay(250).slideDown('fast');
            });
      }
      // Use fallback behavior and write target as content to the overlay wrapper.
      else {
        self.body.html(self.content.clone().removeAttr('id').show());
        // Slide down the content:
        setTimeout(function () {
          self.overlay.css('backgroundImage', 'none');
        }, 250);
        self.wrapper.delay(250).slideDown('fast');
      }
    }, //function

    /**
     * Determines the current scroll position in vertical direction.
     *
     * @returns {integer}
     */
    getScrollPosition: function () {
      return window.pageYOffset || (
          document.documentElement && document.documentElement.scrollTop
          ? document.documentElement.scrollTop
          : (document.body.scrollTop || 0)
      );
    } //function
  });
})(window, jQuery);
