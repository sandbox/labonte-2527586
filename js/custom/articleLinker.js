articleLinker = function () {};

articleLinker.prototype.init = function(parentSelector, slideDuration) {
    this._parentSelector = parentSelector ? parentSelector : 'body';
    this._shareLinks = jQuery(this._parentSelector + ' a.share-link');
    this._directlinkLinks = jQuery(this._parentSelector + ' a.directlink-link');
    this._tagboxLinks = jQuery(this._parentSelector + ' a.edutags-toggle');
    this._slideDuration = slideDuration ? slideDuration : 200;
    this._initShareLinks();
    this._initDirectlinkLinks();
    this._initTagboxLinks();
    this._initDirectlinkInputs();
};

/**
 * Inits the input fields containing the direct link s.t. text is selected on click/focus
 * @returns {void}
 */
articleLinker.prototype._initDirectlinkInputs = function() {
    jQuery('.row.directlink input').on('focus click', function(){this.select();});
};

/**
 * Inits the links to the sharrif element
 * @returns {void}
 */
articleLinker.prototype._initShareLinks = function () {
    var me = this;
    jQuery(this._shareLinks).each(function (idx, shareLink) {
        jQuery(shareLink).click(function (e) {
            me._onShareLinkClick(e, shareLink);
        });
    });
};

articleLinker.prototype._initTagboxLinks = function() {
    var me = this;
    jQuery(this._tagboxLinks).each(function (idx, tagboxLink) {
        jQuery(tagboxLink).click(function (e) {
            me._onTagboxLinkClick(e, tagboxLink);
        });
    });
};

articleLinker.prototype._initDirectlinkLinks = function () {
    var me = this;
    jQuery(this._directlinkLinks).each(function (idx, directlinkLink) {
        jQuery(directlinkLink).click(function (e) {
            me._onDirectlinkLinkClick(e, directlinkLink);
        });
    });
};

articleLinker.prototype._selectDirectLink = function (container) {
    if (jQuery(container).css('display') !== 'none') {
        jQuery(container).find('input')[0].select();
    }
};

articleLinker.prototype._onTagboxLinkClick = function(e, tagboxLink) {
    e.preventDefault();
    jQuery(this._tagboxContainer(tagboxLink)).slideToggle(this._slideDuration);
};

articleLinker.prototype._onShareLinkClick = function (e, shareLink) {
    e.preventDefault();
    jQuery(this._shareContainer(shareLink)).slideToggle(this._slideDuration);
};

articleLinker.prototype._onDirectlinkLinkClick = function (e, directlinkLink) {
    e.preventDefault();
    var me = this;
    var container = this._directlinkContainer(directlinkLink);
    jQuery(container).slideToggle(this._slideDuration, function () {
        me._selectDirectLink(container);
    });
    jQuery(container).find('input')[0].select();
};

articleLinker.prototype._shareContainer = function (shareLink) {
    return jQuery(shareLink).closest('.row').nextAll('.row.share');
};

articleLinker.prototype._directlinkContainer = function (directlinkLink) {
    return jQuery(directlinkLink).closest('.row').nextAll('.row.directlink');
};

articleLinker.prototype._tagboxContainer = function (tagboxLink) {
    return jQuery(tagboxLink).closest('.row').find('.tagbox');
};

