var globals = {
    blockScroll: function() {
        jQuery('body').css({overflow: 'hidden', height: '100%'});
    },
    unblockScroll: function() {
        jQuery('body').css({overflow: 'auto', height: 'auto'});
    },
    
    showOverlay : function(zIndex)
    {
        var overlay = $('<div id="overlay"></div>');
        $(overlay).css(
                {"zIndex": zIndex, backgroundColor: "black", opacity: 0.5, position: "fixed",
                width: "100%", height: "100%", display: "none"});
        $('body').prepend(overlay);
        $(overlay).fadeIn();
    },
    
    hideOverlay : function()
    {
        var overlay = $("#overlay");
        $(overlay).fadeOut(400, function(){$(overlay).remove();});
    }
};

