/**
 * Represents the mobile menu with customized settings, slider and required event bindings
 * @returns {mobileMenu}
 * @requires Uses the mmenu jquery plugin and the slick carousel
 */
mobileMenu = function () {
    this._menu = jQuery('#mobilemenu');
    this._sliderId = '#mm-slick-slider';
};

/**
 * Initializes the mobile menu
 * @param {function()} onOpen A callback executed before menu is opened
 * @param {function()} onClose A callback executed before menu is closed
 * @returns {void}
 */
mobileMenu.prototype.init = function (onOpen, onClose) {
    this._openCallback = onOpen;
    this._closeCallback = onClose;
            
    this._sliderInitialized = false;
    
    var title = this._firstMenuParent().data('title');
    //the slider must be removed from the menu, and then be re-added as navbar
    var sliderHtml = this._extractSlider();
    jQuery(this._menu).mmenu({
        slidingSubmenus: true,
        //extensions: ["effect-fade-menu", "effect-slide-listitems"],
        navbar: {title: title},
        navbars: [{
                position: "top",
                content: [sliderHtml]
            },
            {
                position: "bottom",
                content: ["close"]
            }],
        onClick: {setSelected: true},
        extensions: ["border-offset", "pageshadow", "pagedim-black"],
        offCanvas: {position: "right", zposition: "front"}
    });
    this._api = jQuery("#mobilemenu").data("mmenu");
    this._bindEvents();
};
/**
 * Binds events to the mmenu (not yet implemented)
 * @returns {void}
 */
mobileMenu.prototype._bindEvents = function () {
    var me = this;
    this._api.bind("opened", function(){me._onOpened();});
    this._api.bind("closed", function(){me._onClosed();});
    this._api.bind("open", function () {me._onOpen();});
    this._api.bind("close", function(){me._onClose();});
    $(window).resize(function() {me._api.close();});
};


mobileMenu.prototype._onOpen = function () {
    if (this._openCallback) {
        this._openCallback();
    }
    this._initSlider();
};

mobileMenu.prototype._onClose = function () {
    if (this._closeCallback) {
        this._closeCallback();
    }
};
/**
 * Function executed when the menu is opened
 */
mobileMenu.prototype._onOpened = function () {
    globals.blockScroll();
};


/**
 * Function executed when the menu is closed
 */
mobileMenu.prototype._onClosed = function () {
    globals.unblockScroll();
};

/**
 * Removes slider and returns its html
 * @returns {string} returns the html of the slick slider and removes it from DOM
 */
mobileMenu.prototype._extractSlider = function () {
    var html = jQuery(this._sliderId)[0].outerHTML;
    jQuery(this._sliderId).remove();
    return html;
};

/**
 * Initializes the slick slider
 */
mobileMenu.prototype._initSlider = function () {
    //re-init slider every time the menu is opened prevents slider glitches
    if (this._sliderInitialized) {
        jQuery(this._sliderId).slick('unslick');
    }
    jQuery(this._sliderId).slick({
        slidesToShow: 7,
        slidesToScroll: 2,
        infinite: false,
        responsive: [
            {
                breakpoint: 420,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1
                }
            }
        ]
    });
    this._sliderInitialized = true;
    this._initSliderSubs();
};


mobileMenu.prototype._menuParents = function() {
    return $(this._sliderId).find('[data-menu-id]');
};

mobileMenu.prototype._firstMenuParent = function() {
    var parents = this._menuParents();
    if (parents.length === 0) {
        return null;
    }
    return $(parents[0]);
};

mobileMenu.prototype._initSliderSubs = function() {
    var triggers = $(this._menuParents());
    var me = this;
    
    $(triggers).each(function(idx, trigger){
        $(trigger).click(function(e){e.preventDefault(); e.stopPropagation(); me._activateParent(trigger);});
    });
    if (triggers.length > 0) {
        this._activateParent(this._firstMenuParent());
    }
};

mobileMenu.prototype._activateParent = function(trigger) {
    $(this._menuParents()).removeClass('selected-parent');
    $(trigger).addClass('selected-parent');
    //adjust mmenu navbar title
    var newTitle = $(trigger).data('title');
    $('.mm-title').html(newTitle);
    var parentId = $(trigger).data('menu-id');
    var subSelector = '[data-menu-parent-id="' + parentId + '"]';
    $('[data-menu-parent-id]').not(subSelector).hide();
    $(subSelector).show();
};




